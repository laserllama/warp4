//  testModel.swift
//  warpTests
//
//  Created by Michael DeHaan on 5/12/21.
//

import Foundation
import XCTest

//@testable import warp4

class WarpModelTests: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func getTestSet() -> WarpSet {
        let scene1 = WarpScene(name: "scene1")
        let scene2 = WarpScene(name: "scene2")
        let scenes = [ scene1, scene2 ]
        
        let device1 = WarpDevice(name: "device1")
        let device2 = WarpDevice(name: "device2")
        let devices = [ device1, device2 ]
        
        let instrument1 = WarpInstrument(name: "instrument1", deviceName: "device1")
        let instrument2 = WarpInstrument(name: "instrument2", deviceName: "device2")
        let instruments = [ instrument1, instrument2 ]
        
        let track1 = WarpTrack(name: "track1", instrumentName: "instrument1")
        let track2 = WarpTrack(name: "track2", instrumentName: "instrument2")
        let tracks = [ track1, track2 ]
        
        let scale1 = WarpScale(name: "c_major", root: WarpNote(name: NoteName.C, octave: 2), scaleType: ScaleType.major)
        let scale2 = WarpScale(name: "user_scale", root: WarpNote(name: NoteName.C, octave: 3), scaleType: ScaleType.harmonicMinor)
        let scales = [ scale1, scale2 ]
    

        let p1 = WarpPattern(name: "p1")
        let p2 = WarpPattern(name: "p2")
        let patterns = [ p1, p2 ]
        
        let clip1 = WarpClip(name: "c1", trackName: "track1")
        let clip2 = WarpClip(name: "c2", trackName: "track2")
        scene1.clips.append(clip1)
        scene2.clips.append(clip2)
                
        let song = WarpSong(name: "Good Song", scaleName: "c_major")
        song.scales = scales
        song.tracks = tracks
        song.patterns = patterns
        song.scenes = scenes
        let songs = [ song ]
        
        let set = WarpSet(name: "Set1")
        set.devices = devices
        set.instruments = instruments
        set.songs = songs
        return set
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func testSerialization() throws {
        
        let testSet = getTestSet()
        let json: String = testSet.to_json()!
        let obj = WarpSet.from_json(data: json)!
        XCTAssert(obj.name == "Set1")
        XCTAssert(obj.songs[0].name == "Good Song")
    }
    
    func testRelativeNamedObjectLookups() throws {
        // Warp models saved composition objects as a tree, so in-links between the tree are kept as strings to avoid a serializing a graph representation to JSON
        // this has the effect of requiring all names to be unique for each object type inside a particular song object
        // this test just makes sure we can hop between objects as needed for the runtime engine to work.
        // testing is not required for everything that has an ownership relationship, just the cross-links where there is not direct object ownership in the tree
        
        let testSet = getTestSet()
        let song = testSet.songs[0]
        //let scenes = song.scenes
        let clips = song.scenes[0].clips
        //let tracks = song.tracks
        let track = song.tracks[0]
        //let trackName = clips[0].trackName
        let devices = testSet.devices
        let device = devices[0]
        
        //let instruments = testSet.instruments
        let instrument = testSet.instruments[0]
        
        //runtimeContext.set = testSet
        //runtimeContext.song = song
        
        let trackObjLookup = clips[0].getTrack(song: song)
        let deviceObjLookup =  instrument.getDevice(set: testSet)
        let instrumentObjLookup = track.getInstrument(set: testSet)
        let patternObjLookups1 = clips[0].getPatterns(song: song)

        XCTAssertEqual(track, trackObjLookup)
        XCTAssertEqual(device, deviceObjLookup)
        XCTAssertEqual(instrument, instrumentObjLookup)
        XCTAssertEqual(patternObjLookups1.count, 1)
        
        // FIXME: TODO: add tests for all the .nameExists/.rename(newName)/.delete functions we've added
        // FIXME: scaleName is new on most object types
        // FIXME: add WarpArp (Arp has slots...)
    }
    
    func testRenames() {
    }
    
    func testDeletes() {
    }


    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}


