//
//  testNoteMath.swift
//  warpTests
//
//  Created by Michael DeHaan on 5/16/21.
//

import Foundation

import XCTest
//@testable import warp4

class TestEngine: XCTestCase {
    
    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
    }

    func testNotes() {
        let engine = WarpEngine()
        //print("beep!!")
        let now = NSDate.timeIntervalSinceReferenceDate
        //print("initial start:", now)
        for x in 1...20 {
            let t_start : Double = (Double(x)*0.25) + now
            engine.addNoteToQueue(device: "IAC Driver IAC Bus 1", channel: 1, note: 0x3D, velocity: 110, start: t_start, duration: 0.125)
        }
        
        //sleep(2)
        // var previous = now
        for _ in 1...3000 {
            //var loop_now = NSDate.timeIntervalSinceReferenceDate
            //let delta = loop_now - previous
            //print("delta = ", delta)
            engine.playDueEvents()
            usleep(2000)
            // previous = loop_now
            //loop_now = NSDate.timeIntervalSinceReferenceDate
            
            
        }
        //sleep(2)
        engine.playDueEvents()
        usleep(3)
        // FIXME: need a function/parameter to stop ALL notes
        engine.playDueEvents()
    }
         
    func testThread() {
        let engine = WarpEngine()
        let thread = engine.engage()
        thread.cancel()
    }
    
    func notePlayback() throws {


    }
}

