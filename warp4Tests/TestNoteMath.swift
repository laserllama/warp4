//
//  testNoteMath.swift
//  warpTests
//
//  Created by Michael DeHaan on 5/16/21.
//

import Foundation

import XCTest
//@testable import warp4

class TestNoteMath: XCTestCase {
    
    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
    }

         
    func testBasicTranspositionFromC4() throws {

        let c4 = WarpNote(name: NoteName.C, octave: 4)
        
        XCTAssertEqual(c4.transpose(steps: 0.5, octaves: 0), WarpNote(name: NoteName.Db, octave: 4))
        XCTAssertEqual(c4.transpose(steps: 1, octaves: 0), WarpNote(name: NoteName.D, octave: 4))
        XCTAssertEqual(c4.transpose(steps: 2, octaves: 0), WarpNote(name: NoteName.E, octave: 4))
        XCTAssertEqual(c4.transpose(steps: -0.5, octaves: 0), WarpNote(name: NoteName.B, octave: 3))
        XCTAssertEqual(c4.transpose(steps: -1, octaves: 0), WarpNote(name: NoteName.Bb, octave: 3))
        XCTAssertEqual(c4.transpose(steps: 0, octaves: 3), WarpNote(name: NoteName.C, octave: 7))
        XCTAssertEqual(c4.transpose(steps: 0, octaves: -3), WarpNote(name: NoteName.C, octave: 1))
        
    }
    
    func testBasicTranspositionFromB4() throws {
        
        let b4 = WarpNote(name: NoteName.B, octave: 4)

        XCTAssertEqual(b4.transpose(steps: 0.5, octaves: 0), WarpNote(name: NoteName.C, octave: 5))
        XCTAssertEqual(b4.transpose(steps: 1, octaves: 0), WarpNote(name: NoteName.Db, octave: 5))
        XCTAssertEqual(b4.transpose(steps: -0.5, octaves: 0), WarpNote(name: NoteName.Bb, octave: 4))
        XCTAssertEqual(b4.transpose(steps: -1, octaves: 0), WarpNote(name: NoteName.A, octave: 4))
    }
    
    func testScales() throws {
        
        // FIXME: scale types should be enums
        let s1 = WarpScale(name: "C Major", root: WarpNote(name: NoteName.C, octave: 0), scaleType: ScaleType.major)
        let s2 = WarpScale(name: "Eb Pentatonic", root: WarpNote(name: NoteName.Eb, octave: 0), scaleType: ScaleType.pentatonic)
        
        let r1 = s1.root
        let r2 = s2.root
        
        XCTAssertEqual(r1, WarpNote(name: NoteName.C, octave: 0))
        
        XCTAssertEqual(r2, WarpNote(name: NoteName.Eb, octave: 0))

        // C major tests
        let s1_0 = r1.scaleTranspose(scale: s1, steps: 0)!
        let s1_1 = r1.scaleTranspose(scale: s1, steps: 1)!
        let s1_2 = r1.scaleTranspose(scale: s1, steps: 2)!
        let s1_3 = r1.scaleTranspose(scale: s1, steps: 3)!
        let s1_4 = r1.scaleTranspose(scale: s1, steps: 4)!
        let s1_5 = r1.scaleTranspose(scale: s1, steps: 5)!
        let s1_6 = r1.scaleTranspose(scale: s1, steps: 6)!
        let s1_7 = r1.scaleTranspose(scale: s1, steps: 7)!
        //let s1_8 = r1.scaleTranspose(scale: s1, steps: 8)!

        
        XCTAssertEqual(s1_0, WarpNote(name: NoteName.C, octave: 0))
        XCTAssertEqual(s1_1, WarpNote(name: NoteName.D, octave: 0))
        XCTAssertEqual(s1_2, WarpNote(name: NoteName.E, octave: 0))
        XCTAssertEqual(s1_3, WarpNote(name: NoteName.F, octave: 0))
        XCTAssertEqual(s1_4, WarpNote(name: NoteName.G, octave: 0))
        XCTAssertEqual(s1_5, WarpNote(name: NoteName.A, octave: 0))
        XCTAssertEqual(s1_6, WarpNote(name: NoteName.B, octave: 0))
        XCTAssertEqual(s1_7, WarpNote(name: NoteName.C, octave: 1))

        
        // Eb pentatonic minor tests
    }

}

