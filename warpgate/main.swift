//
//  main.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/2/21.
//

import Foundation
import Darwin.ncurses
import OSLog

WarpLogger.log(message: "Warp Sequencer - http://warspeq.com")
WarpLogger.log(message: "(C) Michael DeHaan <michael@michaeldehaan.net> 2021")
WarpLogger.log(message: "")
WarpLogger.log(message: "reticulating splines...")
WarpLogger.log(message: "dynotherms connected.")
WarpLogger.log(message: "all systems go.")
WarpLogger.log(message: "")


WarpRuntimeContext.set = WarpSet()
let set = WarpRuntimeContext.set

//WarpSet.createInitialSet()

let engine = WarpEngine()

for _ in 0 ... 12 {
    let _ = set.addNewSong()
}

set.injectDevices(engine: engine)

let interface = InterfaceLoop(set: set)


interface.go()

signal(SIGINT) { signal in endwin(); exit(0) }

