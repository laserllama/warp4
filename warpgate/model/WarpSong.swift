//  Created by Michael DeHaan on 5/12/21.

import Foundation

class WarpSong : Encodable, Decodable, Browseable {

    var name: String
    var tempo: Int = 120
    var scenes: [WarpScene] = []
    var tracks: [WarpTrack] = []
    var scales: [WarpScale] = []
    var patterns: [WarpPattern] = []
    var arps: [WarpArp] = []
    var autoAdvanceScenes: Bool = true
    var scaleName: String
    
    init(name: String, scaleName: String) {
        self.name = name
        self.scaleName = scaleName
    }
    
    func getInterfaceDisplayString() -> String {
        return self.name
    }
    
    func addDefaultScale() -> WarpScale {
        
        let scaleName = "default"
        // FIXME: scaleTypes should probably be enums
        let defaultScale : WarpScale = WarpScale(name: scaleName, root: WarpNote(name: NoteName.C, octave: 0), scaleType: ScaleType.major)
        self.scales.append(defaultScale)
        self.scaleName = scaleName
        return defaultScale
    }
    
    // so ... BOOKMARK!!!
    // we need to make sure this is called when we make a new song
    // and make sure it cannot be selected on the song screen
    
    func addInheritScale() -> WarpScale {
        let scaleName = "inherit"
        // FIXME: scaleTypes should probably be enums
        let inheritScale : WarpScale = WarpScale(name: scaleName, root: WarpNote(name: NoteName.C, octave: 0), scaleType: ScaleType.inherit)
        self.scales.append(inheritScale)
        return inheritScale
    }
    
    private func getNewScaleName() -> String {
        var counter = 1
            
        while(true) {
            let name = "scale\(counter)"
            if (!self.scales.contains(where: { $0.name == name })) { return name }
            counter += 1
        }
    }
    
    func addNewScale() -> WarpScale {
        let scaleName : String  = getNewScaleName()
        let newScale = WarpScale(name: scaleName, root: WarpNote(name: NoteName.C, octave: 0), scaleType: ScaleType.major)
        self.scales.append(newScale)
        return newScale
    }
    
    private func getNewArpName() -> String {
        var counter = 1
            
        while(true) {
            let name = "arp\(counter)"
            if (!self.arps.contains(where: { $0.name == name })) { return name }
            counter += 1
        }
    }
    
    func addNewArp() -> WarpArp {
        let arpName : String  = getNewArpName()
        let newArp = WarpArp(name: arpName)
        self.arps.append(newArp)
        return newArp
    }
    
    private func getNewTrackName() -> String {
        var counter = 1
            
        while(true) {
            let name = "track\(counter)"
            if (!self.tracks.contains(where: { $0.name == name })) { return name }
            counter += 1
        }
    }
        
    func addNewTrack(set: WarpSet) -> WarpTrack {
        // requires instruments for this to be safe to call
        let trackName : String = getNewTrackName()
        let instrumentName : String = set.instruments.first!.name
        let newTrack : WarpTrack = WarpTrack(name: trackName, instrumentName: instrumentName)
        self.tracks.append(newTrack)
        
        for scene in self.scenes {
            scene.normalizeClips(song: self)
        }
        
        return newTrack
    }
    
    private func getNewSceneName() -> String {
        var counter = 1
            
        while(true) {
            let name = "scene\(counter)"
            if (!self.scenes.contains(where: { $0.name == name })) { return name }
            counter += 1
        }
    }
        
    func addNewScene(set: WarpSet) -> WarpScene {
        let sceneName : String = getNewSceneName()
        let newScene : WarpScene = WarpScene(name: sceneName)
        self.scenes.append(newScene)
        return newScene
    }
    
    private func getNewPatternName() -> String {
        var counter = 1
            
        while(true) {
            let name = "pattern\(counter)"
            if (!self.patterns.contains(where: { $0.name == name })) { return name }
            counter += 1
        }
    }
    
    func addNewPattern() -> WarpPattern {
        let patternName : String  = getNewPatternName()
        let newPattern = WarpPattern(name: patternName)
        self.patterns.append(newPattern)
        return newPattern
    }
        
    static func nameExists(set: WarpSet, newName: String) -> Bool {
        return set.songs.contains(where: { $0.name == newName })
    }
    
    func getScene(name: String?) -> WarpScene? {
        if (name == nil) { return nil }
        return self.scenes.first(where: { $0.name == name })
    }
    
    func getTrack(name: String?) -> WarpTrack? {
        if (name == nil) { return nil }
        return self.tracks.first(where: { $0.name == name })
    }
    
    func getScale(name: String?) -> WarpScale? {
        if (name == nil) { return nil }
        return self.scales.first(where: { $0.name == name })
    }
    
    func getPattern(name: String?) -> WarpPattern? {
        if (name == nil) { return nil }
        return self.patterns.first(where: { $0.name == name })
    }
    
    func getArp(name: String?) -> WarpArp? {
        if (name == nil) { return nil }
        return self.arps.first(where: { $0.name == name })
    }

}
