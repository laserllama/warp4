//  WarpPattern.swift
//  warp
//
//  Created by Michael DeHaan on 5/17/21.
//
//  A musical scale, based on a given root note and a scale type, which can be calculated
//  to return the list of notes it the scale. For transposition math, see the Note class.
//  Scales can be assigned to many objects in Warp and have a precedence/override order.

import Foundation

class WarpScale : Browseable, Encodable, Decodable {

    var name: String
    var root: WarpNote
    var scaleType: ScaleType
    
    // internal-ish
    var scaleSteps: [ScaleDegree]
    var cache: [WarpNote] = []

    init(name: String, root: WarpNote, scaleType: ScaleType) {
        self.name = name
        self.root = root
        self.scaleType = scaleType
        // TODO: user defined scales could have steps passed in from a different constructor
        //print("scale type is: ", scaleType)
        if (self.scaleType != ScaleType.inherit) {
           self.scaleSteps = SCALE_TYPE_STEPS[scaleType]!
           self.cache = self.generateInternalScaleCache()
        } else {
            self.scaleSteps = []
            self.cache = []
        }
    }
    
    func getInterfaceDisplayString() -> String {
        return self.name
    }
    
    static func nameExists(song: WarpSong, newName: String) -> Bool {
        return song.scales.contains(where: { $0.name == newName }) == true
    }

    static func canDelete(song: WarpSong, scale: WarpScale) -> Bool {
        // can't delete is the song is using the scale
        if (song.scaleName == scale.name) { return false }
        // can't delete if a clip is using the scale
        for scene in song.scenes {
            for clip in scene.clips {
                if (clip.scaleName == scale.name) {
                    return false
                }
            }
        }
        // can't delete if a pattern is using the scale
        for pattern in song.patterns {
            if (pattern.scaleName == scale.name) {
                return false
            }
        }
        return true
    }
    
    static func delete(song: WarpSong, scale: WarpScale) {
        let deleteIndex: Int = song.scales.firstIndex(where: { $0.name == scale.name })!
        song.scales.remove(at: deleteIndex)
    }
    
    /* FIXME: on the song, add canDeleteScale/deleteScale, etc */
    
    func rename(song: WarpSong, newName: String) -> Void {
        if (song.scaleName == self.name) { song.scaleName = newName }
        for scene in song.scenes {
            if (scene.scaleName == self.name) { scene.scaleName = newName }
            scene.clips.forEach({ if ($0.scaleName == self.name) { $0.scaleName = newName } })
        }
        self.name = newName
    }
        
    private func generateInternalScaleCache(length: Int = 120) -> [WarpNote] {

        var counter = length
        var octave_shift = 0
        var index = 0;
        var cache : [WarpNote] = []

        while counter > 0 {
            
            let numericSteps = self.scaleSteps[index]
            let note = self.root.transpose(steps: numericSteps.rawValue, octaves: octave_shift)
            cache.append(note)
            
            index = index + 1
            counter = counter - 1
            
            if index >= self.scaleSteps.count {
                index = 0
                octave_shift = octave_shift + 1
            }
        }
        
        return cache
        
        
    }
    
}

