//
//  WarpClip.swift
//  warp
//
//  Created by Michael DeHaan on 5/17/21.
//
// An arpeggiator, which is made up of slots.

import Foundation


class WarpArp : Browseable, Encodable, Decodable, HasSlots {

    var name: String
    var slots: [WarpSlot] = []
    var length: Int = 16
    var speed: Double = 1.0
    var octaveShift: Int = 0
    var auditionPatternName : String = "-"
    var auditionInstrumentName : String = "-"

    
    // FIXME: song should contain a list of arps, add to model tests
    
    init(name: String) {
        self.name = name
        for _ in 0 ... 63 {
            self.slots.append(WarpSlot())
        }
    }
    
    func isPercussion() -> Bool {
        return false
    }
    
    func getSlot(_ position: Int) -> WarpSlot {
        return self.slots[position]
    }
    
    func setSlot(_ position: Int, _ value: WarpSlot) -> Void {
        self.slots[position] = value
    }
    
    func getInterfaceDisplayString() -> String {
        return self.name
    }
    
    static func nameExists(song: WarpSong, newName: String) -> Bool {
        return song.arps.contains(where: { $0.name == newName }) == true
    }

    func rename(song: WarpSong, newName: String) -> Void {
        for scene in song.scenes {
            for clip in scene.clips {
                if (clip.arp1Name == self.name) { clip.arp1Name = newName }
                if (clip.arp2Name == self.name) { clip.arp2Name = newName }
                if (clip.arp3Name == self.name) { clip.arp3Name = newName }
                if (clip.arp4Name == self.name) { clip.arp4Name = newName }

            }
        }
        self.name = newName
    }
    
    func copy(newName: String) -> WarpArp {
        let newArp = WarpArp(name: newName)
        newArp.length = self.length
        newArp.slots = self.slots.map({ $0.copy() })
        newArp.speed = self.speed
        newArp.octaveShift = self.octaveShift
        newArp.auditionPatternName = self.auditionPatternName
        newArp.auditionInstrumentName = self.auditionInstrumentName
        return newArp
    }
    
    
    static func canDelete(song: WarpSong, arp: WarpArp) -> Bool {
        for scene in song.scenes {
            for clip in scene.clips {
                if ((clip.arp1Name == arp.name) || (clip.arp2Name == arp.name) || (clip.arp3Name == arp.name) || (clip.arp4Name == arp.name)) {
                   return false
                }
            }
        }
        return true
    }
    
    static func delete(song: WarpSong, arp: WarpArp) -> Void {
        song.arps.removeAll(where: { $0.name == arp.name })
    }
    
}
