//
//  WarpClip.swift
//  warp
//
//  Created by Michael DeHaan on 5/17/21.
//
//  A Clip, which is owned by a Scene, references a Track, and contains Clips.

import Foundation

class WarpClip : Browseable, Encodable, Decodable {
    
    var pattern1Name : String = "-"
    var pattern2Name : String = "-"
    var pattern3Name : String = "-"
    var pattern4Name : String = "-"
    
    var arp1Name : String = "-"
    var arp2Name : String = "-"
    var arp3Name : String = "-"
    var arp4Name : String = "-"
    
    var trackName: String = ""
    var scaleName: String = "inherit"
    var tempoMultiplier : Double = 1.0
    
    init(trackName: String) {
        self.trackName = trackName
    }
    
    func getPatterns(song: WarpSong)  -> [ WarpPattern ]{
        
        var results : [ WarpPattern ] = []
                
        let p1 = song.getPattern(name: pattern1Name)
        let p2 = song.getPattern(name: pattern2Name)
        let p3 = song.getPattern(name: pattern3Name)
        let p4 = song.getPattern(name: pattern4Name)
        
        if (p1 != nil)  { results.append(p1!) }
        if (p2 != nil)  { results.append(p2!) }
        if (p3 != nil ) { results.append(p3!) }
        if (p4 != nil)  { results.append(p4!) }

        return results
        
    }
    
    func getArps(song: WarpSong) -> [ WarpArp ] {
        var results : [ WarpArp ] = []
                
        let a1 = song.getArp(name: arp1Name)
        let a2 = song.getArp(name: arp2Name)
        let a3 = song.getArp(name: arp3Name)
        let a4 = song.getArp(name: arp4Name)
        
        if (a1 != nil)  { results.append(a1!) }
        if (a2 != nil)  { results.append(a2!) }
        if (a3 != nil ) { results.append(a3!) }
        if (a4 != nil)  { results.append(a4!) }

        return results
    }
    
    func getInterfaceDisplayString() -> String {
        
        var patternNames : [String] = []
        var properties : [String] = []
        var propertyString : String = ""
        
        if (pattern1Name != "-") {
            patternNames.append(pattern1Name)
        }
        if (pattern2Name != "-") {
            patternNames.append(pattern2Name)
        }
        if (pattern3Name != "-") {
            patternNames.append(pattern3Name)
        }
        if (pattern4Name != "-") {
            patternNames.append(pattern4Name)
        }
        
        var patternString : String = patternNames.joined(separator: ",")
        if (patternNames.count == 0) {
            patternString = "(empty)"
        }
                
        properties.append("@\(tempoMultiplier)x)")
        if (properties.count > 0) {
            propertyString = properties.joined(separator: " ")
        } else {
            propertyString = ""
        }
                
        //return String(format: "%30s:      %20s", arguments: [self.trackName, propertyString])
        return "\(trackName) : \(patternString) \(propertyString)"

    }
    
    func getTrack(song: WarpSong) -> WarpTrack? {
        return song.getTrack(name: self.trackName)
    }
    

}
