//  WarpPattern.swift
//  warp
//
//  Created by Michael DeHaan on 5/17/21.
//
//  A Pattern is a set of Slots that can be repeated in a Clip
//  A Pattern may have a scale and a tempo multiplier.

import Foundation


class WarpPattern : Browseable, Encodable, Decodable, HasSlots {

    var name: String
    var slots: [WarpSlot]
    var scaleName: String = "inherit"
    var length: Int = 16
    var tempoMultiplier: Double = 1.0
    var percussion : Bool = false
    var octaveShift: Int = 0
    var oneShot: Bool = false
    var auditionInstrumentName : String = "-"

    init(name: String) {
        self.name = name
        self.slots = []
        for _ in 0...63 {
            self.slots.append(WarpSlot())
        }
    }
    
    func getSlot(_ position: Int) -> WarpSlot {
        return self.slots[position]
    }
    
    func setSlot(_ position: Int, _ value: WarpSlot) -> Void {
        self.slots[position] = value
    }
    
    func getInterfaceDisplayString() -> String {
        return self.name
    }

    static func nameExists(song: WarpSong, newName: String) -> Bool {
        return song.patterns.contains(where: { $0.name == newName }) == true
    }

    func rename(song: WarpSong, newName: String) -> Void {
        for scene in song.scenes {
            for clip in scene.clips {
                if (clip.pattern1Name == self.name) { clip.pattern1Name = newName }
                if (clip.pattern2Name == self.name) { clip.pattern2Name = newName }
                if (clip.pattern3Name == self.name) { clip.pattern3Name = newName }
                if (clip.pattern4Name == self.name) { clip.pattern4Name = newName }

            }
        }
        self.name = newName
    }
    
    func copy(newName: String) -> WarpPattern {
        let newPattern = WarpPattern(name: newName)
        newPattern.length = length
        newPattern.scaleName = scaleName
        newPattern.slots = slots.map({ $0.copy() })
        newPattern.tempoMultiplier = tempoMultiplier
        newPattern.octaveShift = octaveShift
        newPattern.percussion = percussion
        newPattern.oneShot = oneShot
        newPattern.auditionInstrumentName = auditionInstrumentName
        return newPattern
    }
    
    func isPercussion() -> Bool {
        return self.percussion
    }
    
    static func canDelete(song: WarpSong, pattern: WarpPattern) -> Bool {
        for scene in song.scenes {
            for clip in scene.clips {
                if ((clip.pattern1Name == pattern.name) || (clip.pattern2Name == pattern.name) || (clip.pattern3Name == pattern.name) || (clip.pattern4Name == pattern.name)) {
                   return false
                }
            }
        }
        return true
    }
    
    static func delete(song: WarpSong, pattern: WarpPattern) -> Void {
        song.patterns.removeAll(where: { $0.name == pattern.name })
    }
    
}
