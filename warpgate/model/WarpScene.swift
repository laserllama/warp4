//  Created by Michael DeHaan on 5/12/21.

//  WarpPattern.swift
//  warp
//
//  Created by Michael DeHaan on 5/17/21.
//
//  A Scene is a list of Clips (each with their own track) that are meant to be
//  played together at the same time.

import Foundation


class WarpScene : Browseable, Encodable, Decodable {

    var name: String
    var clips: [WarpClip] = []
    var scaleName: String = "inherit"
    var tempoMultiplier: Double = 1.0
    var bars: Int = 4
    var enabled: Bool = true

    init(name: String) {
        self.name = name
    }
    
    func getInterfaceDisplayString() -> String {
        return self.name
    }
    
    func normalizeClips(song: WarpSong) -> Void {
        for track in song.tracks {
            if !(self.clips.contains(where: { $0.trackName == track.name })) {
                self.clips.append(WarpClip(trackName: track.name))
            }
        }
    }
    
    static func nameExists(song: WarpSong, newName: String) -> Bool {
        return song.scenes.contains(where: { $0.name == newName })
    }

    func rename(song: WarpSong, newName: String) -> Void {
        // nothing references scene by name, so nothing to check
        self.name = newName
    }
    
    func delete(song: WarpSong) -> Void {
        // nothing references scene by name, so nothing to do
        return
    }

}
