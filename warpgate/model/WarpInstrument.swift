//
//  WarpInstrument.swift
//  warp
//
//  Created by Michael DeHaan on 5/17/21.
//
//  An instrument ties a MIDI device and channel together, and is how a Track
//  routes notes to be played by the Engine.

import Foundation

class WarpInstrument : Browseable, Encodable, Decodable, Equatable {

    var name: String
    var deviceName: String
    var channel: Int = 0
    var selectedMidiCCs : [Int] = [ 0, 1, 2, 3 ]
    var muted: Bool = false
    var percussion: Bool = false
    var percussionMap: [WarpNote] = [
        WarpNote(name: NoteName.C, octave: 2), // 0
        WarpNote(name: NoteName.C, octave: 2), // 1
        WarpNote(name: NoteName.C, octave: 2), // 2
        WarpNote(name: NoteName.C, octave: 2), // 3
        WarpNote(name: NoteName.C, octave: 2), // 4
        WarpNote(name: NoteName.C, octave: 2), // 5
        WarpNote(name: NoteName.C, octave: 2), // 6
        WarpNote(name: NoteName.C, octave: 2), // 7
        WarpNote(name: NoteName.C, octave: 2), // 8
        WarpNote(name: NoteName.C, octave: 2), // 9
    ]
    var baseOctave: Int = 0
    
    init(name: String, deviceName: String) {
        self.name = name
        self.deviceName = deviceName
    }
    
    func getInterfaceDisplayString() -> String {
        return self.name
    }
    
    // FIXME: simplify the code for all of these to use forEach and compactMap more
    
    static  func nameExists(set: WarpSet, newName: String) -> Bool {
        return set.instruments.contains(where: { $0.name == newName }) == true
    }
    
    static func nameAvailable(set: WarpSet, newName: String) -> Bool {
        return !(self.nameExists(set: set, newName: newName))
    }
    
    func rename(set: WarpSet, newName: String) -> Void {
        for song in set.songs {
            song.tracks.forEach({ if ($0.instrumentName == self.name) { $0.instrumentName = newName } })
        }
        self.name = newName
    }
    
    func delete(set: WarpSet) -> Void {
        // removing the instrument removes any tracks that reference the instrument
        for song in set.songs {
            var newTracks : [WarpTrack] = []
            for track in song.tracks {
                if (track.instrumentName != self.name) {
                    newTracks.append(track)
                }
            }
            song.tracks = newTracks
        }
    }
    
    func getDevice(set: WarpSet) -> WarpDevice? {
        return set.getDevice(name: self.deviceName)
    }
    
}

func ==(lhs: WarpInstrument, rhs: WarpInstrument) -> Bool {
    return (lhs.name == rhs.name) && (lhs.deviceName == rhs.deviceName)
}

