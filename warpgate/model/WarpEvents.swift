//
//  WarpEvents.swift
//  warp
//
//  Created by Michael DeHaan on 5/17/21.
//

import Foundation

class WarpEvents {
    
    var onEvents: [ WarpOnEvent ] = []
    var offEvents: [ WarpOffEvent ] = []
    // var ccEvents: [ WarpCcEvents ] = []

    init(onEvents: [WarpOnEvent], offEvents: [WarpOffEvent]) {
        self.onEvents = onEvents
        self.offEvents = offEvents
    }
}
