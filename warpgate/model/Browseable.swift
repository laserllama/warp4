//
//  Browseable.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/12/21.
//

import Foundation

protocol Browseable {
    
    func getInterfaceDisplayString() -> String
    
}
