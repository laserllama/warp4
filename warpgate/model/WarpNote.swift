//
//  WarpNote.swift
//  warp
//
//  Created by Michael DeHaan on 5/16/21.
//
//  a basic Note object, which can be transposed absolutely or inside a Scale, and can return its MIDI note number.

import Foundation


class WarpNote : Encodable, Decodable, Equatable {
    
    var name : NoteName
    var octave : Int = 0
    
    init(name: NoteName, octave: Int) {
        self.name = name
        self.octave = octave
    }
    
    func isEqual(object: AnyObject?) -> Bool {
        // I think this is needed for XTCAssertEquals since it doesn't use ==
        // possibly can change the tests and remove this method
        guard let other = object as? WarpNote else { return false }
        return self == other
    }
    
    func scaleTranspose(scale: WarpScale, steps: Int) -> WarpNote? {
        /// given a scale and a number of scale steps as an interval, return a new note offset by that many steps on the scale
        /// if the starter note is not in the scale, or too many steps are requested in either direction, returns nil
        let index : Int? = scale.cache.firstIndex(where: { $0.name == self.name && $0.octave == self.octave })
        if index == nil {
            return nil
        }
        let newIndex : Int = index! + steps
        if (newIndex < 0) {
            return nil
        }
        return scale.cache[newIndex]
    }
    
    func transpose(steps: Double = 0, octaves: Int = 0) -> WarpNote {
        /// given a number of steps (1=whole tone, 0.5=semitone) or octaves, return the note offset by that amount
        /// this can return notes with "unplayable" octaves beyond the MIDI note number range
            
        var counter : Double = steps;
        var newName : NoteName = self.name
        var newOctave : Int = self.octave
        let intSteps : Int = Int(steps)
        var currentIndex : Int = NOTES.firstIndex(of: newName)!
        
        if (counter > 0 && counter != Double(intSteps)) {
            // add a half step
            if (self.name == NoteName.B) { newOctave += 1; newName = NoteName.C }
            else { newName = NOTES[currentIndex + 1] }
            counter -= 0.5 // record we did it
        }
        
        if (counter < 0 && counter != Double(intSteps)) {
            // remove a half step
            if (self.name == NoteName.C) { newOctave -= 1; newName = NoteName.B }
            else { newName = NOTES[currentIndex - 1] }
            counter += 0.5 // record we did it
        }
        
        while (counter > 0) {
            // need to add a whole step
            if (newName == NoteName.B) { newOctave += 1; newName = NoteName.Db }
            else if (newName == NoteName.Bb) { newOctave += 1; newName = NoteName.C }
            else { currentIndex = NOTES.firstIndex(of: newName)!; newName = NOTES[currentIndex + 2] }
            counter -= 1.0
        }
        
        while (counter < 0) {
            // need to remove a whole step
            if (newName == NoteName.C) { newOctave -= 1; newName = NoteName.Bb }
            else if (newName == NoteName.Db) { newOctave += 1; newName = NoteName.B }
            else { currentIndex = NOTES.firstIndex(of: newName)!; newName = NOTES[currentIndex - 2] }
            counter += 1.0
        }
        
        return WarpNote(name: newName, octave: newOctave + octaves)
    }
    
    func noteNumber() -> Int {
        /// return the MIDI note number for the note.
        /// TODO: make this return nil if the notes are outside the usable range
        return NOTES.firstIndex(of: self.name)! + (12 * self.octave)
    }
    
}

func ==(lhs: WarpNote, rhs: WarpNote) -> Bool {
    return (lhs.octave == rhs.octave) && (lhs.name == rhs.name)
}


