//
//  WarpConstants.swift
//  warp
//
//  Created by Michael DeHaan on 5/16/21.
//

import Foundation

let DEFAULT_VELOCITY = 120

enum NoteName: String, Codable {
    case C = "C"
    case D = "D"
    case Db = "Db"
    case E = "E"
    case Eb = "Eb"
    case F = "F"
    case G = "G"
    case Gb = "Gb"
    case A = "A"
    case Ab = "Ab"
    case B = "B"
    case Bb = "Bb"
}

enum ScaleDegree: Double, Codable { /* values are steps and half steps */
     case SD_1  = 0 // tonic
     case SD_b2 = 0.5
     case SD_2  = 1
     case SD_b3 = 1.5
     case SD_3  = 2
     case SD_4  = 2.5
     case SD_b5 = 3
     case SD_5  = 3.5
     case SD_b6 = 4
     case SD_6  = 4.5
     case SD_b7 = 5
     case SD_7  = 5.5
     case SD_8  = 6
}

enum SharpFlat: String, Codable {
    case SHARP  = "#"
    case NORMAL = " "
    case FLAT = "b"
}

let SHARP_FLATS : [ SharpFlat] = [ SharpFlat.SHARP, SharpFlat.NORMAL, SharpFlat.FLAT ]

let SHARP_FLAT_NAMES = SHARP_FLATS.map { $0.rawValue }

let NOTES : [ NoteName ] = [ NoteName.C,  NoteName.Db, NoteName.D, NoteName.Eb, NoteName.E, NoteName.F, NoteName.Gb, NoteName.G, NoteName.Ab, NoteName.A, NoteName.Bb, NoteName.B ]

let NOTE_NAME_STRINGS = NOTES.map { $0.rawValue }

let NOTE_NAME_STRINGS_WITH_NONE = [ "-", "C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "Bb", "B" ]

enum TieType : String, Codable {
    case On = "tie"
    case Off = "-"
    case Mute = "mute"
}

let TIE_TYPES : [TieType] = [ TieType.Mute, TieType.Off, TieType.On]

let TIE_TYPE_NAMES : [String] = TIE_TYPES.map { $0.rawValue }

enum ChordType : String, Codable {
    case off = "off"
    case major = "M"
    case minor = "m"
    case diminished = "dim"
    case augmented = "aug"
    case sus4 = "sus4"
    case sus2 = "sus2"
    case fourth = "4"
    case fifth = "5"
    case M6 = "M6"
    case m6 = "m6"
    case dom7 = "dom7"
    case M7 = "M7"
    case m7 = "m7"
    case aug7 = "aug7"
    case dim7 = "dim7"
    case mM7 = "mM7"
}

let CHORD_STEPS: [ ChordType: [Int]] = [
    ChordType.off        : [ ],
    ChordType.major      : [4, 7],
    ChordType.minor      : [3, 7],
    ChordType.diminished : [3, 6],
    ChordType.augmented  : [4, 8],
    ChordType.sus4       : [ 5, 7],
    ChordType.sus2       : [ 2, 7],
    ChordType.fourth     : [ 5 ],
    ChordType.fifth      : [ 7 ],
    ChordType.M6         : [ 4, 7, 9],
    ChordType.m6         : [ 3, 7, 9],
    ChordType.dom7       : [ 4, 7, 10],
    ChordType.M7         : [ 4, 7, 11 ],
    ChordType.m7         : [ 3, 7, 10 ],
    ChordType.aug7       : [4, 8, 10],
    ChordType.dim7       : [ 3, 6, 10],
    ChordType.mM7        : [ 3, 7, 11]
]

let CHORD_TYPES = [
    ChordType.off,
    ChordType.major,
    ChordType.minor,
    ChordType.diminished,
    ChordType.augmented,
    ChordType.sus4,
    ChordType.sus2,
    ChordType.fourth,
    ChordType.fifth,
    ChordType.M6,
    ChordType.m6,
    ChordType.dom7,
    ChordType.M7,
    ChordType.m7,
    ChordType.aug7,
    ChordType.dim7,
    ChordType.mM7
]

let CHORD_TYPE_NAMES: [String] = CHORD_TYPES.map({ $0.rawValue })

enum ScaleType : String, Codable {
    case inherit = "inherit"
    case major = "major"
    case pentatonic = "pentatonic"
    case pentatonicMinor = "pentatonic minor"
    case naturalMinor = "natural minor"
    case blues = "blues"
    case chromatic = "chromatic"
    case dorian = "dorian"
    case harmonicMajor = "harmonic major"
    case harmonicMinor = "harmonic minor"
    case locrian = "locrian"
    case lydian = "lydian"
    case majorPentatonic = "major pentatonic"
    case melodicMinorAsc = "melodic minor asc"
    case melodicMinorDesc = "melodic minor desc"
    case minorPentatonic = "minor pentatonic"
    case mixolydian = "mixolydian"
    case phrygian = "phrygian"
    case japanese = "japanese"
    case akebono = "akebono"
}

let SCALE_TYPE_STEPS : [ ScaleType: [ScaleDegree] ] = [
    ScaleType.major             : [ ScaleDegree.SD_1, ScaleDegree.SD_2, ScaleDegree.SD_3, ScaleDegree.SD_4, ScaleDegree.SD_5, ScaleDegree.SD_6, ScaleDegree.SD_7 ], // ionian
    ScaleType.pentatonic        : [ ScaleDegree.SD_1, ScaleDegree.SD_2, ScaleDegree.SD_3, ScaleDegree.SD_5, ScaleDegree.SD_6 ],
    ScaleType.pentatonicMinor   : [ ScaleDegree.SD_1, ScaleDegree.SD_3, ScaleDegree.SD_4, ScaleDegree.SD_5, ScaleDegree.SD_7 ],
    ScaleType.naturalMinor      : [ ScaleDegree.SD_1, ScaleDegree.SD_2, ScaleDegree.SD_b3, ScaleDegree.SD_4, ScaleDegree.SD_5, ScaleDegree.SD_b6, ScaleDegree.SD_b7 ], // aeolian
    ScaleType.blues             : [ ScaleDegree.SD_1, ScaleDegree.SD_b3, ScaleDegree.SD_4, ScaleDegree.SD_b5, ScaleDegree.SD_5, ScaleDegree.SD_b7 ],
    ScaleType.dorian            : [ ScaleDegree.SD_1, ScaleDegree.SD_2, ScaleDegree.SD_b3, ScaleDegree.SD_4, ScaleDegree.SD_5, ScaleDegree.SD_6, ScaleDegree.SD_b7 ],
    ScaleType.chromatic         : [ ScaleDegree.SD_1, ScaleDegree.SD_b2, ScaleDegree.SD_2, ScaleDegree.SD_b3, ScaleDegree.SD_3, ScaleDegree.SD_4, ScaleDegree.SD_b5,
                             ScaleDegree.SD_5, ScaleDegree.SD_b6, ScaleDegree.SD_6, ScaleDegree.SD_b7, ScaleDegree.SD_7 ],
    ScaleType.harmonicMajor     : [ ScaleDegree.SD_1, ScaleDegree.SD_2, ScaleDegree.SD_3, ScaleDegree.SD_4, ScaleDegree.SD_5, ScaleDegree.SD_b6, ScaleDegree.SD_7 ],
    ScaleType.harmonicMinor     : [ ScaleDegree.SD_1, ScaleDegree.SD_2, ScaleDegree.SD_3, ScaleDegree.SD_4, ScaleDegree.SD_5, ScaleDegree.SD_b6, ScaleDegree.SD_7 ],
    ScaleType.locrian           : [ ScaleDegree.SD_1, ScaleDegree.SD_b2, ScaleDegree.SD_b3, ScaleDegree.SD_4, ScaleDegree.SD_b5, ScaleDegree.SD_b6, ScaleDegree.SD_b7 ],
    ScaleType.lydian            : [ ScaleDegree.SD_1, ScaleDegree.SD_2, ScaleDegree.SD_3, ScaleDegree.SD_b5, ScaleDegree.SD_5, ScaleDegree.SD_6, ScaleDegree.SD_7 ],
    ScaleType.majorPentatonic   : [ ScaleDegree.SD_1, ScaleDegree.SD_2, ScaleDegree.SD_3, ScaleDegree.SD_5, ScaleDegree.SD_6 ],
    ScaleType.melodicMinorAsc   : [ ScaleDegree.SD_1, ScaleDegree.SD_2, ScaleDegree.SD_b3, ScaleDegree.SD_4, ScaleDegree.SD_5, ScaleDegree.SD_6, ScaleDegree.SD_7 ],
    ScaleType.melodicMinorDesc  : [ ScaleDegree.SD_1, ScaleDegree.SD_2, ScaleDegree.SD_b3, ScaleDegree.SD_4, ScaleDegree.SD_5, ScaleDegree.SD_b6, ScaleDegree.SD_b7 ],
    ScaleType.minorPentatonic   : [ ScaleDegree.SD_1, ScaleDegree.SD_b3, ScaleDegree.SD_4, ScaleDegree.SD_5, ScaleDegree.SD_b7 ],
    ScaleType.mixolydian        : [ ScaleDegree.SD_1, ScaleDegree.SD_2, ScaleDegree.SD_3, ScaleDegree.SD_4, ScaleDegree.SD_5, ScaleDegree.SD_6, ScaleDegree.SD_b7 ],
    ScaleType.phrygian          : [ ScaleDegree.SD_1, ScaleDegree.SD_b2, ScaleDegree.SD_b3, ScaleDegree.SD_4, ScaleDegree.SD_5, ScaleDegree.SD_b6, ScaleDegree.SD_b7 ],
    ScaleType.japanese          : [ ScaleDegree.SD_1, ScaleDegree.SD_2, ScaleDegree.SD_4, ScaleDegree.SD_5, ScaleDegree.SD_6 ],
    ScaleType.akebono           : [ ScaleDegree.SD_1, ScaleDegree.SD_2, ScaleDegree.SD_b3, ScaleDegree.SD_5, ScaleDegree.SD_6 ]
]

let SCALE_TYPES : [ScaleType] = [
    ScaleType.inherit,
    ScaleType.major,
    ScaleType.pentatonic,
    ScaleType.pentatonicMinor,
    ScaleType.naturalMinor , // aeolian
    ScaleType.blues,
    ScaleType.dorian,
    ScaleType.chromatic,
    ScaleType.harmonicMajor,
    ScaleType.harmonicMinor,
    ScaleType.locrian,
    ScaleType.lydian,
    ScaleType.majorPentatonic,
    ScaleType.melodicMinorAsc,
    ScaleType.melodicMinorDesc,
    ScaleType.minorPentatonic,
    ScaleType.mixolydian,
    ScaleType.phrygian,
    ScaleType.japanese,
    ScaleType.akebono
]

let SCALE_TYPE_NAMES : [String] = SCALE_TYPES.map { $0.rawValue }

let TEMPO_MULTIPLIER_CHOICES = [ 0.125, 0.25, 0.5 , 1,  2,  4, 8 ]

let ARP_SPEED_CHOICES = [ 0.0625, 0.125, 0.25, 0.5 , 1,  2,  4, 8, 16 ]
