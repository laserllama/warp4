//
//  WarpOffEvent.swift
//  warp
//
//  Created by Michael DeHaan on 5/17/21.
//
//  A MIDI Note Off Event

import Foundation

struct WarpOffEvent {
    
    var time : Double = 0
    var device : String = ""
    var channel : UInt8 = 0
    var note : UInt8 = 0

    init(device: String, channel: UInt8, note: UInt8, time: Double) {
        self.device = device
        self.channel = channel
        self.note = note
        self.time = time
    }
}
