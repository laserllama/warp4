//
//  HasSlots.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/20/21.
//

import Foundation

//
//  Browseable.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/12/21.
//

import Foundation

protocol HasSlots {
    
    func getSlot(_ position: Int) -> WarpSlot
    
    func setSlot(_ position: Int, _ value: WarpSlot) -> Void
 
    func isPercussion() -> Bool
    
}
