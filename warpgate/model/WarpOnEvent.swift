//
//  WarpOnEvent.swift
//  warp
//
//  Created by Michael DeHaan on 5/17/21.
//
//  A MIDI Note On Event

import Foundation

struct WarpOnEvent {
    
    var time : Double = 0
    var device : String = ""
    var channel : UInt8 = 0
    var note : UInt8 = 0
    var velocity : UInt8 = 0
    
    init(device: String, channel: UInt8, note: UInt8, velocity: UInt8, time: Double) {
        self.device = device
        self.channel = channel
        self.note = note
        self.velocity = velocity
        self.time = time
    }
}
