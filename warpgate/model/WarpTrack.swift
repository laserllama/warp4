//  Created by Michael DeHaan on 5/12/21

import Foundation


class WarpTrack : Browseable, Encodable, Decodable, Equatable {

    var name: String
    var instrumentName: String
    var muted: Bool = false

    init(name: String, instrumentName: String) {
        self.name = name
        self.instrumentName = instrumentName
    }
        
    func getInterfaceDisplayString() -> String {
        return self.name
    }

    func getInstrument(set: WarpSet) -> WarpInstrument? {
        return set.getInstrument(name: self.instrumentName)
    }
    
    static func nameExists(song: WarpSong, newName: String) -> Bool {
        return song.tracks.contains(where: { $0.name == newName })
        
    }
    
    func rename(song: WarpSong, newName: String) -> Void {
        
        // FIXME: depending on the playhead implementation this may need to also update WarpConductor ...
        // FIXME: this should probably defer more logic to WarpScene

        for scene in song.scenes {
            scene.clips.forEach({ if $0.trackName == name { $0.trackName = newName }})
        }
        self.name = newName
    }
    
    func delete(song: WarpSong) -> Void {
        
        // FIXME: this should probably defer more logic to WarpScene
        
        var newClips : [WarpClip] = []
        for scene in song.scenes {
            for clip in scene.clips {
                if !(clip.trackName == self.name) {
                    newClips.append(clip)
                }
            }
            scene.clips = newClips
        }
    }
    
    
}

func ==(lhs: WarpTrack, rhs: WarpTrack) -> Bool {
   return lhs.name == rhs.name
}
