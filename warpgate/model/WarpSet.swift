//  WarpPattern.swift
//  warp
//
//  Created by Michael DeHaan on 5/17/21.
//
//  A WarpSet is like a musical set, a collection of songs.  Device and Instrument configurations
//  are also saved with the set, not with the song, to prevent them to need to be configured/reconfigured
//  each time a new song is started. Users could get by with having only one set file if they wanted. Sets
//  are the file that Warp saves/loads, so this is the most top-level object in Warp's object tree.

import Foundation

final class WarpSet : Encodable, Decodable {

    var name: String
    var filename: String
    var devices: [WarpDevice] = []
    var songs: [WarpSong] = []
    var instruments: [WarpInstrument] = []

    enum CodingKeys: String, CodingKey {
        case name, filename, devices, songs, instruments
    }
    
    init() {
        self.name = "set1"
        self.filename = "set1.warp"
        self.devices = []
        self.songs = []
        self.instruments = []
    }
    
    init(name: String) {
        self.name = name
        self.filename = "\(name).warp"
    }
    
    func getDevice(name: String?) -> WarpDevice? {
        if (name == nil) { return nil }
        return self.devices.first(where: { $0.name == name })
    }
    
    func injectDevices(engine: WarpEngine) {
        for deviceName in engine.getDeviceNames() {
            if (!(self.devices.contains(where: { $0.name == deviceName }))) {
                self.devices.append(WarpDevice(name: deviceName))
            }
        }
    }
    
    func getSong(name: String?) -> WarpSong? {
        if (name == nil) { return nil }
        return self.songs.first(where: { $0.name == name })
    }
        
    private func getNewSongName() -> String {
        var songCounter = 1
            
        while(true) {
            let songName = "song\(songCounter)"
            if (!self.songs.contains(where: { $0.name == songName })) { return songName }
            songCounter += 1
        }
    }
    
    func addNewSong() -> WarpSong {
        let songName : String  = getNewSongName()
        let newSong = WarpSong(name: songName, scaleName: "default")
        self.songs.append(newSong)
        let _ = newSong.addDefaultScale()
        let _ = newSong.addInheritScale()
        return newSong
    }
    
    func getInstrument(name: String?) -> WarpInstrument? {
        if (name == nil) { return nil }
        return self.instruments.first(where: { $0.name == name })
    }
    
    func addNewInstrument() -> WarpInstrument? {
        var instrumentCounter = 1
        
        var firstDevice : WarpDevice?
        
        if set.devices.count > 0 {
            firstDevice = set.devices[0]
        } else {
            // need to make devices first
            return nil
        }
        
        while(true) {
            let instrumentName = "instrument\(instrumentCounter)"
            if (!self.instruments.contains(where: { $0.name == instrumentName })) {
                let newInstrument = WarpInstrument(name: instrumentName, deviceName: firstDevice!.name)
                self.instruments.append(newInstrument)
                return newInstrument
            }
            instrumentCounter += 1
        }
    }
    
    /*
    func nameExists(newName: String) -> Bool {
        // any name is fine
        return false
    }
    
    func rename(newName: String) -> Void {
        self.name = newName
    }
    
    func delete() -> Void {
        return
    }
    */
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        filename = try values.decode(String.self, forKey: .filename)
        devices = try values.decode([WarpDevice].self, forKey: .devices)
        songs = try values.decode([WarpSong].self, forKey: .songs)
        instruments = try values.decode([WarpInstrument].self, forKey: .instruments)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(filename, forKey: .filename)
        try container.encode(devices, forKey: .devices)
        try container.encode(songs, forKey: .songs)
        try container.encode(instruments, forKey: .instruments)
    }
    
    static func from_json(data: String) -> WarpSet? {
        let decoder = JSONDecoder()
        do {
            let obj = try decoder.decode(WarpSet.self, from: data.data(using: .utf8)!)
            return obj
        }
        catch {
            return nil
        }
    }
    
    func to_json() -> String? {
        do {
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let data = try encoder.encode(self)
            return String(data: data, encoding: .utf8)
        }
        catch {
            return nil
        }
    }
            
    func save() -> Void {
        self.filename = "\(self.name).warp"
        saveAs(filename: self.filename)
    }
    
    func saveAs(filename: String) -> Void {
        WarpStorageManager.saveAs(set: self, filename: filename)
    }
    
    static func createInitialSet() -> WarpSet {
        
            /* FIXME: this is the set Warp starts up with... */
        
            let set = WarpSet(name: "Set")
            let _ = set.addNewSong()
            return set
        }
    

}
