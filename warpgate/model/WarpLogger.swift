//
//  Logging.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/10/21.
//



class WarpLogger {

    static var logMessages : [String] = []
    static var retainLogs : Int = 5000
    
    static func log(message: String) {
        WarpLogger.logMessages.append(message)
        if (WarpLogger.logMessages.count > WarpLogger.retainLogs) {
            WarpLogger.logMessages.removeFirst()
        }
    }
    
    static func getMessages() -> [String] {
        return WarpLogger.logMessages
    }
    
}
