//
//  WarpSlot.swift
//  warp
//
//  Created by Michael DeHaan on 5/17/21.
//
//  A WarpSlot is a position in a pattern that can be evaluated to WarpNotes (or Events) based on the current context.
//  basically this means the slot will evaluate into a note, chord, or a set of repeated notes.  There may be associated
//  MIDI CC information (Warp supports a number of configurable modulation slots on each instrument) and MIDI velocity information.
//  Velocity = 0 is not the same as muting a step since the velocity could be routed to something like an LFO speed.


import Foundation

class WarpSlot : Encodable, Decodable {
    
    // these should all belong to Slot:
    var absoluteNote : String
    var scaleNote : Int
    var sharpFlat : SharpFlat
    var randomScaleNote: Int
    var chordType: ChordType
    var octaveShift : Int
    var randomOctaveShift : Int
    var tie : TieType
    var tieChance: Int // 0-100
    // var length : Double = 0.25
    //var startTime : Double = 0
    //var endTime : Double = 0
    //var fromScale : WarpScale? = nil
    var repeats : Int
    var randomRepeats : Int
    var noteChance: Int // 0-100
    var velocity : Int
    var randomVelocity: Int
    var midiCCs : [ Int ]
    var randomMidiCCs : [ Int ]
    
    // FIXME: add chordTypes
    // FIXME: add noteChance
    // FIXME: add randomVelocity
    // FIXME: add
    
    init() {
        self.scaleNote = 0 // 0 means "off", 1 is tonic, anything else is tonic +/-
        self.absoluteNote = "-" // "-" means off, percussion only, otherwise a note name
        self.randomScaleNote = 0 // shifts the scale note up or down
        self.sharpFlat = SharpFlat.NORMAL
        self.chordType = ChordType.off
        self.octaveShift = 0
        self.randomOctaveShift = 0
        self.tie = TieType.Off // this also includes mutes since they would be exclusive with ties
        self.tieChance = 0
        self.repeats = 0
        self.randomRepeats = 0
        self.noteChance = 100 // aka mute chance
        self.velocity = DEFAULT_VELOCITY
        self.midiCCs = [0,0,0,0] // UI/engine constraints: decided to limit midi CCs to four choices per instrument
        self.randomVelocity = 0
        self.randomMidiCCs = [0,0,0,0]
    }
    
    func copy() -> WarpSlot {
        let newSlot = WarpSlot()
        newSlot.scaleNote = self.scaleNote
        newSlot.absoluteNote = self.absoluteNote
        newSlot.sharpFlat = self.sharpFlat
        newSlot.randomScaleNote = self.randomScaleNote
        newSlot.chordType = self.chordType
        newSlot.octaveShift = self.octaveShift
        newSlot.randomOctaveShift = self.randomOctaveShift
        newSlot.tie = self.tie
        newSlot.tieChance = self.tieChance
        newSlot.repeats = self.repeats
        newSlot.randomRepeats = self.randomRepeats
        newSlot.noteChance = self.noteChance
        newSlot.velocity = self.velocity
        newSlot.randomVelocity = self.randomVelocity
        newSlot.midiCCs = self.midiCCs.map { $0 }
        newSlot.randomMidiCCs = self.midiCCs.map { $0 }
        return newSlot
    }
    
    /* FIXME: this needs to take into account all of the above, and currently does not... it should return an array of notes */
    
    func getNote(scale : WarpScale) -> WarpNote? {
        if (self.scaleNote == 0) {
            // muted
            return nil
        }
        let base = scale.root.scaleTranspose(scale: scale, steps: self.scaleNote)
        return base?.transpose(steps: 0, octaves: self.octaveShift)
    }
    
    /*
     
    // FIXME: should this take a scene or clip or song or should there be three methods?  Probably the latter
     
    func getEvents(runtimeContext: WarpRuntimeContext) -> WarpEvents {
        
        // FIXME: not implemented yet, return the notes
        let onEvents : [WarpOnEvent] = []
        let offEvents : [WarpOffEvent] = []
        
        return WarpEvents(onEvents: onEvents, offEvents: offEvents)
    }
    */
    
}



