//
//  WarpRuntimeContext.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/13/21.
//

import Foundation

class WarpRuntimeContext {
    
    static var set : WarpSet = WarpSet.createInitialSet()
    
    static func quit() -> Void {
        endwin()
        exit(0)
    }
    
    // FIXME:  UI needs to call this.
    // FIXME:  call stop (if not panic) using WarpConductor
    
    static func changeSet(newSet: WarpSet) {

        WarpRuntimeContext.set = newSet
    }
    
}
