//  WarpDevice.swift
//  warp
//
//  Created by Michael DeHaan on 5/17/21.
//
//  A Device, an object that represents a MIDI device such as a MIDI-USB interface or an IAC Bus.

import Foundation


public class WarpDevice : Browseable, Encodable, Decodable, Equatable {

    var name: String
    var muted: Bool = false
    
    init(name: String) {
       self.name = name
    }
    
    func getInterfaceDisplayString() -> String {
        return self.name
    }
        
    static func nameExists(set: WarpSet, newName: String) -> Bool {
        // devices cannot be renamed, so this probably won't be called
        return set.devices.contains(where: { $0.name == newName }) == true
    }

    func rename(set: WarpSet, newName: String) -> Void {
        // devices cannot be renamed since they come from the operating system
        // this will be handled by not making the option available in any UI.
    }
    
    /* FIXME: this can't go here, we actually have to block the delete and handle it... method should go in WarpSet. */
    /*
    func delete(set: WarpSet) -> Void {
        set.instruments.forEach({ if ($0.deviceName == self.name) { $0.deviceName = nil } })
    }
    */
    
}

public func ==(lhs: WarpDevice, rhs: WarpDevice) -> Bool {
    return (lhs.name == rhs.name)
}

