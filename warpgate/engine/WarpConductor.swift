//
//  WarpConductor.swift
//  warp
//
//  Created by Michael DeHaan on 5/20/21.
//
//  The conductor is a high level class that presents what should be playing
//  and enqueues notes on the WarpEngine (the low level MIDI handling code).
//
//  FIXME: this is not finished or tested. Ideally this system should be implemented
//  using multiple callbacks, one that works with WarpEngine, and another that can
//  simply queue events to a list, so there can be unit tests.

import Foundation

//let TRANSITION_WAIT: Double = 0.05  // seconds to wait before checking again that a scene has stopped before trying to queue the next

class WarpConductor {
    
    var engine: WarpEngine
    
    // FIMXE: adding a track will also
    
    var song : WarpSong?
    var callbacks: WarpCallbackManager
    var playheadForTrack: [String : WarpPlayhead]
    
    init(engine: WarpEngine, callbacks: WarpCallbackManager) {
        self.engine = WarpEngine()
        self.callbacks = callbacks
        self.song = nil
        self.playheadForTrack = [:]
    }
    
    // FIXME: Song class needs to use this
    func addTrack(track: WarpTrack) {
        playheadForTrack[track.name] = WarpPlayhead(conductor: self, track: track, engine: engine)
    }
    
    // FIXME: Song class needs to use this
    func removeTrack(track: WarpTrack) {
        playheadForTrack.removeValue(forKey: track.name)
    }
    
    // FIXME:  Song class needs to use this
    func renameTrack(song: WarpSong, track: WarpTrack, newName: String) {
        if (track.name == newName) { return }
        let playhead = playheadForTrack[track.name]
        playheadForTrack.removeValue(forKey: track.name)
        playheadForTrack[newName] = playhead
        track.rename(song: song, newName: newName)
    }
    
    // FIXME: UI will need to call this when changing songs
    func changeToSong(song: WarpSong) {
        self.song = song
        playheadForTrack = [:]
        for track in song.tracks {
            self.addTrack(track: track)
        }
    }
    
    func playFirstScene() {
        if self.song == nil { return }
        if self.song!.scenes.count == 0 { return }
        let first : WarpScene? = self.song!.scenes.first
        if (first !=  nil) {
            self.playScene(scene: first!)
        }
    }
    
    func playScene(scene: WarpScene) {
        for playhead in self.allPlayheads() {
            playhead.stopLoopingAtEndOfBar()
        }
        for clip in scene.clips {
            self.playClip(clip: clip)
        }
    }
    
    func allPlayheads() -> [WarpPlayhead] {
        return self.playheadForTrack.values.map { $0 }
    }
    
    func getPlayheadForClip(clip: WarpClip) -> WarpPlayhead? {
        for (trackName, playhead) in self.playheadForTrack {
            if (clip.trackName == trackName) {
                return playhead
            }
        }
        return nil
    }
    
    func playClip(clip: WarpClip) -> Void {
        let playhead : WarpPlayhead? = self.getPlayheadForClip(clip: clip)
        if (playhead == nil) { return; }
        playhead!.stopLoopingAtEndOfBar()
        playhead!.loadClip(clip: clip)
    }
    
    // FIXME: this isn't going to be exposed in the UI, so commenting this out to avoid possibilities of dead code living in the codebase.
    /*
    func stopClip(clip: WarpClip) -> Void {
        let playhead : WarpPlayhead? = self.getPlayheadForClip(clip: clip)
        if (playhead == nil) { return; }
        playhead!.stop() // FIXME: includes find the off events in the engine queue and accelerate their due date
    }
    */
    
    func stopAll() -> Void {
        for playhead in self.allPlayheads() {
            playhead.stop()
        }
    }
    
    func allInstruments() -> [WarpInstrument] {
        return WarpRuntimeContext.set.instruments
    }
    
    func midiPanic() -> Void {
        for instrument in self.allInstruments() {
            self.engine.panic(instrument: instrument)
        }
    }
    
}
