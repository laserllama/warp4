//
//  WarpEngine.swift
//  warp
//
//  Created by Michael DeHaan on 5/17/21.
//
//  The Engine contains functions for playing MIDI notes and some time-based queues for
//  Event objects. The Engine is controlled by the WarpStreamer.

import Foundation
import CoreMIDI

class WarpEngine {

    // map of MIDI device names to device numbers
    var midiDeviceMap : [ String : Int ] = [:]
    
    // list of on and off events to play, these will be sorted by time
    var on_events : [WarpOnEvent] = []
    var off_events : [WarpOffEvent] = []
    
    // background thread and lock used for realtime MIDI playback engine
    var thread1 : Thread?
    let condition1 = NSCondition()
    
    // coremidi details
    var client : MIDIClientRef = 0
    var port : MIDIPortRef = 0


    init() {
                
        // get the MIDI device names and numbers
        self.midiDeviceMap = WarpEngine.getMidiDeviceMap()
            
        // if there is at least one MIDI device, set up CoreMIDI for all devices
        if self.midiDeviceMap.count > 0 {
            MIDIClientCreate("WarpSeq Test Client" as CFString, nil, nil, &self.client)
            MIDIOutputPortCreate(self.client, "WarpSeq Test OutPort" as CFString, &self.port)
       }
        
    }
    
    func getDeviceNames() -> [String] {
        return self.midiDeviceMap.keys.sorted()
    }
    
    func engage() -> Thread {
        // starts the background thread that can play MIDI events as they are due
        // FIXME: make this do what we actually want.
        // FIXME: see comments below.
        let thread1 = Thread(target: self, selector: #selector(self.forThreadOne), object: nil)
        thread1.start()
        return thread1
    }
    
    @objc func forThreadOne() {
        // FIXME: this is temporary debug code - right now the loop does not occur on a thread at
        // all, implying the threading needs to be moved OUT of this class, and then we can use
        // this class from within a thread. ALTERNATIVELY, this class could still manage it's own
        // thread but engage() maybe should be a static method.
        // .playDueEvents() is the method to use
        
        for i in 1 ... 7 {
            
            print("locking")
            // this is just a trial demo of the lock class
            condition1.lock()
            // condition_1.wait() - stay asleep until condition_1.signal
            print("thread running")
            print(i)
            condition1.unlock()
            print("unlocked")
            
        }
        return // exit thread
        
    }

    private static func getMidiObjectDisplayName(_ obj: MIDIObjectRef) -> String {
        // what's the MIDI device name?
        // borrowed from https://stackoverflow.com/questions/28924831/coremidi-callbacks-in-swift
        // this function is used by getMidiDeviceMap() below
        var param: Unmanaged<CFString>?
        var capturedName = "Error"
        let err = MIDIObjectGetStringProperty(obj, kMIDIPropertyDisplayName, &param)
        if err == OSStatus(noErr) {
            capturedName = param!.takeRetainedValue() as String
        }
        return capturedName
    }

    private static func getMidiDeviceMap() -> [String:Int] {
        // returns a dictionary of MIDI device names to CoreMIDI device numbers
        
        var name_map : [String:Int] = [:]
        let count:Int = MIDIGetNumberOfDestinations()

        for i in 0..<count {
            let endpoint:MIDIEndpointRef = MIDIGetDestination(i)
            if  endpoint != 0 {
                name_map[WarpEngine.getMidiObjectDisplayName(endpoint)] = i
            }
        }
        return name_map
    }
    
    func makePacket(channel: UInt8, note: UInt8, velocity: UInt8 = 127, on: Bool) -> MIDIPacket {
        // return a new on or off MIDI packet
        //   channel --- the numerical midi channel
        //   note ------ the MIDI note number
        //   velocity -- MIDI velocity number
        //   on -------- true for an on note, false for off.
        var midiPacket:MIDIPacket = MIDIPacket()
        midiPacket.timeStamp = 0
        midiPacket.length = 3
        midiPacket.data.0 = (UInt8)(  (on ? 0x90 : 0x80) + channel - 1)
        midiPacket.data.1 = note
        midiPacket.data.2 = on ? velocity : 0
        return midiPacket
    }
    
    private func getDestination(device: String) -> MIDIEndpointRef? {
        // get a CoreMIDI destination object from a device name
        let deviceNumber = self.midiDeviceMap[device]
        if deviceNumber != nil {
            return MIDIGetDestination(deviceNumber!)
        } else {
            return nil
        }
    }
    
    func playNoteOn(on_event: WarpOnEvent) {
        // immediately send a MIDI note on event
        let dest = getDestination(device: on_event.device)
        if (dest == nil) { return }
        let on_packet = makePacket(channel: on_event.channel, note: on_event.note, velocity: on_event.velocity, on: true)
        var packetList = MIDIPacketList(numPackets: 1, packet: on_packet)
        MIDISend(self.port, dest!, &packetList)
    }
    
    func playNoteOff(off_event: WarpOffEvent) {
        // immediately send a MIDI note off event
        let dest = getDestination(device: off_event.device)
        if (dest == nil) { return }
        let on_packet = makePacket(channel: off_event.channel, note: off_event.note, velocity: 0, on: false)
        var packetList = MIDIPacketList(numPackets: 1, packet: on_packet)
        MIDISend(self.port, dest!, &packetList)
    }
    
    private func addOnEvent(on_event: WarpOnEvent) {
        // enqueue a MIDI note on event
        // TODO: this may need some locking later.
        on_events.append(on_event)
    }
    
    private func addOffEvent(off_event: WarpOffEvent) {
        // onequeue a MIDI note off event
        // TODO: this may need some locking later.
        off_events.append(off_event)
    }
    
    func addNoteToQueue(device: String, channel: UInt8, note: UInt8, velocity: UInt8, start: Double, duration: Double) {
        // add an on note and a corresponding off note event to the appropriate queues
        let on_packet = WarpOnEvent(device: device, channel: channel, note: note, velocity: velocity, time: start)
        let off_packet = WarpOffEvent(device: device, channel: channel, note: note, time: start + duration)
        on_events.append(on_packet)
        off_events.append(off_packet)
    }
    
    func playDueEvents() {
        // play events that need to be played
        // TODO: intended for use inside a larger event loop manager.
                
        on_events.sort { $0.time < $1.time }
        off_events.sort { $0.time < $1.time }
        
        let interval = NSDate.timeIntervalSinceReferenceDate
        
        var on_count = 0
        var off_count = 0

        for on_event in on_events {
            if on_event.time <= interval {
                playNoteOn(on_event: on_event)
                on_count = on_count + 1
            }
        }
                
        for off_event in off_events {
            if off_event.time <= interval {
                playNoteOff(off_event: off_event)
                off_count = off_count + 1
            }
        }
        
        on_events.removeFirst(on_count)
        off_events.removeFirst(off_count)
        
    }
    
    func panic(instrument: WarpInstrument) {
        // FIXME: implement ... create and send a stop note for every possible note
    }
        
}

