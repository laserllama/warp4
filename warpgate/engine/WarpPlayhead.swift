//
//  WarpPlayhead.swift
//  warp
//
//  Created by Michael DeHaan on 5/20/21.
//

// FIXME: not finished or tested.

import Foundation

class WarpPlayhead {
    
    var conductor : WarpConductor
    var track : WarpTrack
    var engine : WarpEngine
    var paused : Bool
    var position: Double
    
    init(conductor: WarpConductor, track: WarpTrack, engine: WarpEngine) {
        self.paused = true
        self.position = 0
        self.conductor = conductor
        self.track = track
        self.engine = engine
    }
    
    func stopLoopingAtEndOfBar() {
        // switch the end of bar marker from "re-evaluate clip" to "stop"
    }
    
    func stop() {
        // be sure this stops any notes that haven't been stopped
    }
    
    func loadClip(clip: WarpClip) {
        // if not started, start ...
        // add all the events in the clip to the timeline
        // the end of the timeline should include an event to re-evaluate the timeline because of randomness
        // there should be an end of bar marker in the timeline.
    }
    
    
    
    /*
    func pause() {
        self.paused = true
    }
    
    func rewind() {
        self.position = 0
    }
    
    func stop() {
        self.pause()
        self.rewind()
    }
    
    func advance(seconds: Double) {
        // this will usually be called with a small number of milliseconds
        
        if (self.paused) {
            return
        }
        self.enqueueDueEvents(previousTimeIndex: position, newTimeIndex: position + seconds)
        self.position += seconds
    }
    
    func enqueueDueEvents(previousTimeIndex: Double, newTimeIndex: Double) {
        // find all the slots between now and here that have just started
        // look at all pattern.clips
        // compute them down to the events and enqueue them with the Engine
        
    }
    */

    

    
    
}

