//
//  FileManager.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/13/21.
//

import Foundation

class WarpStorageManager {
        
    static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    static func saveAs(set: WarpSet, filename: String) {
        let url = getDocumentsDirectory().appendingPathComponent(filename)
        do {
            try set.to_json()!.write(to: url, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            WarpLogger.log(message: error.localizedDescription)
        }
    }
    
    static func getFilesToOpen() -> [WarpFile] {
        var results : [WarpFile] = []
        do {

            let files : [String] = try FileManager.default.contentsOfDirectory(atPath: getDocumentsDirectory().path)
            for file in files {
                if file.hasSuffix(".warp") {
                    results.append(WarpFile(name: file))
                }
            }
            
        } catch {
            WarpLogger.log(message: error.localizedDescription)
        }
        return results
            
    }
    
    static func open(filename: String) -> WarpSet? { //reading
        let pathUrl = getDocumentsDirectory().appendingPathComponent(filename)
        var text2 : String = ""
        
        do {
            text2 = try String(contentsOf: pathUrl, encoding: String.Encoding.utf8)
        }
        catch {
            WarpLogger.log(message: error.localizedDescription)
            return nil
        }
        
        let newSet : WarpSet? = WarpSet.from_json(data: text2)
        if (newSet != nil) {
            WarpRuntimeContext.changeSet(newSet: newSet!)
            return newSet
        } else {
            return nil
        }
    }
    
}
