//
//  EntryPoint.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//

import Darwin.ncurses

// FIXME: BOOKMARK: making this work mostly on fields, not positions, the fields can now their positions vertically.
// update this and also UiRenderer


class InterfaceLoop {
    
    static let KEY_BACKSPACE: Int32 = 8
    static let KEY_DELETE: Int32 = 127
    static let KEY_LEFT : Int32 = 260
    static let KEY_RIGHT : Int32 = 261
    static let KEY_UP : Int32 = 259
    static let KEY_DOWN : Int32 = 258
    static let KEY_ENTER : Int32 = 13
    static let KEY_ESCAPE : Int32 = 27
    
    var set: WarpSet
    var screen: BaseScreen
    
    var currentField: BaseField?
    var currentFields: [BaseField]
    var currentHeaders: [ScreenLabel]
    
    init(set: WarpSet) {
        self.set = set
        self.screen = SetScreen(set: self.set)
        self.currentFields = []
        self.currentHeaders = []
    }
    
    func loadScreen() {
        
        // FIXME: move this into screen so we can call screen.redraw and it will work

        // get the field and header objects from the screen object
        self.currentFields = self.screen.getFields()
        self.currentHeaders = self.screen.getHeaders()

        // let all fields know what their renderer is so they can redraw themselves
        // FIXME: we might not need this, we might have some other method to get a new
        // display string, if so, can remove later
        
        for (fieldIndex, field) in self.currentFields.enumerated() {
            field.setLocation(screenRow: fieldIndex + 3)
        }
                
        self.currentField = self.currentFields.first
            
        if !self.currentField!.isSelectable() {
            self.currentField = self.screen.getNextField(fields: self.currentFields, currentField: self.currentField!)
        }
        drawScreen()
        
    }
    
    func drawScreen() {
        
        self.screen.drawScreen(headers: self.currentHeaders, fields: self.currentFields)
        
        if (self.currentField != nil) {
            // this can only really happen if a Screen doesn't return any fields, more of a development error
            self.screen.drawCursor(field: currentField)
        }
    }
    
    func redraw() {
        self.screen.drawScreen(headers: self.currentHeaders, fields: self.currentFields)
    }
    
    func onLeft(key: Int32) -> BaseScreen {
        let newScreen : BaseScreen = currentField!.onLeft()
        self.screen.drawCursor(field: self.currentField!)
        return newScreen
    }
    
    func onRight(key: Int32) -> BaseScreen {
        let newScreen : BaseScreen = currentField!.onRight()
        self.screen.drawCursor(field: self.currentField!)
        return newScreen
    }
    
    func onKeyUp(key: Int32) -> BaseScreen {
        //self.screen.onKeyUp()
        
        if currentField!.isLockedWidget() {
            return currentField!.onKeyUpWhenLocked()
        }
        else {
            let newScreen : BaseScreen = currentField!.onLeave()
            let newField : BaseField? = self.screen.getPreviousField(fields: self.currentFields, currentField: currentField!)
            self.screen.drawMovedCursor(fromField: self.currentField!, toField: newField)
            self.currentField = newField
            return newScreen
        }
    }
    
    func onKeyDown(key: Int32) -> BaseScreen {
        //self.screen.onKeyDown()
        if currentField!.isLockedWidget() {
            return currentField!.onKeyDownWhenLocked()
        }
        else {
            let newScreen : BaseScreen = currentField!.onLeave()
            let newField : BaseField? = self.screen.getNextField(fields: self.currentFields, currentField: currentField!)

            self.screen.drawMovedCursor(fromField: self.currentField!, toField: newField)
            currentField = newField
            return newScreen
        }
    }
    
    func onKeyEnter(key: Int32) -> BaseScreen {
        let newScreen : BaseScreen = currentField!.onEnter()
        if currentField!.shouldReloadOnEnter() {
            self.screen = newScreen
            self.loadScreen()
        }
        return newScreen
    }
    
    func onBackspace(key: Int32) -> BaseScreen {
        return currentField!.onBackspace()
    }
    
    func onRegularCharacter(key: Int32) -> BaseScreen {
        // 0-9, A-Z, a-z
        // allow the screen to intercept the key, and if not intercepted, pass to the field
        // this is used to access the help and log overlay
        var newScreen : BaseScreen = self.screen.onRegularCharacter(key: key)
        if (newScreen === self.screen) {
            newScreen = currentField!.onRegularCharacter(key: key)
        }
        return newScreen
    }
    
    func onEscape(key: Int32) -> BaseScreen {
        if (self.screen.back != nil) {
            return self.screen.back!
        }
        return self.screen
    }
    
    func onOtherKey(key: Int32) -> BaseScreen {
        return self.currentField!.onOtherKey(key: key)
    }
    
    func handleKey(key: Int32) -> BaseScreen {
        
        var newScreen : BaseScreen = self.screen
                    
        if (key == InterfaceLoop.KEY_LEFT) {
            newScreen = self.onLeft(key: key)
        }
        else if (key == InterfaceLoop.KEY_RIGHT) {
            newScreen = self.onRight(key: key)
        }
        else if (key == InterfaceLoop.KEY_UP) {
            newScreen = self.onKeyUp(key: key)
        }
        else if (key == InterfaceLoop.KEY_DOWN) {
            newScreen = self.onKeyDown(key: key)
        }
        else if (key == InterfaceLoop.KEY_ENTER) {
            newScreen = self.onKeyEnter(key: key)
        }
        else if ((key == InterfaceLoop.KEY_BACKSPACE) || (key == InterfaceLoop.KEY_DELETE)) {
            newScreen = self.onBackspace(key: key)
        }
        else if (((key >= 48) && (key <= 57 )) || ((key >= 65) && (key <= 90)) || ((key >= 97) && (key <= 122))) {
            newScreen = self.onRegularCharacter(key: key)
        }
        else if (key == InterfaceLoop.KEY_ESCAPE) {
            newScreen = self.onEscape(key: key)
        }
        else {
            newScreen = self.onOtherKey(key: key)
        }
        
        return newScreen
    }
    
    func go() {
        
        self.screen.setup()
        self.loadScreen()
                
        while (true) {
            
            let newScreen: BaseScreen = self.handleKey(key: getch())
            
            if (!(newScreen === self.screen)) {
                self.screen = newScreen
                self.loadScreen()
            } else if (self.screen.invalid) {
                self.drawScreen()
                self.screen.invalid = false
            }
        }
    }
            

}

