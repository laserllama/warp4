//
//  File.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/13/21.
//

import Foundation

class WarpFile : Browseable {
    
    var name : String
    
    init(name: String) {
        self.name = name
    }
    
    func getInterfaceDisplayString() -> String {
        return self.name
    }
}
