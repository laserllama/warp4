//
//  MenuChoice.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/4/21.
//

import Foundation

class BaseField {
    
    var screen: BaseScreen
    var screenRow: Int = 0
    
    var hasValue: Bool = false
    var valueLeftBracket: String = "<"
    var valueRightBracket: String = ">"
    var valueDisplayWidth: Int = 15
    var valueColumn: Int = 30
    
    init(screen: BaseScreen) {
        self.screen = screen
    }
            
    func setLocation(screenRow: Int) {
        self.screenRow = screenRow
    }
    
    func onEscape() -> BaseScreen {
        return self.screen
    }
    
    func onEnter() -> BaseScreen {
        return self.screen
    }
    
    func onLeave() -> BaseScreen {
        return self.screen
    }
    
    func onLeft() -> BaseScreen {
        return self.screen
    }
    
    func onRight() -> BaseScreen {
        return self.screen
    }
    
    func onBackspace() -> BaseScreen {
        return self.screen
    }
    
    func onRegularCharacter(key: Int32) -> BaseScreen {
        return self.screen
    }
    
    func onOtherKey(key: Int32) -> BaseScreen {
        return self.screen
    }
    
    func getDisplayString() -> String {
        return "?"
    }
    
    func getDisplayValue() -> String {
        return ""
    }
    
    func shouldReloadOnEnter() -> Bool {
        return false
    }
    
    func isSelectable() -> Bool {
        return true
    }
    
    func getDisplayHeight() -> Int {
        return 1
    }
    
    func getCustomDisplayHandler() -> ((_ screenRow: Int) -> Void)? {
        return nil
    }
    
    func isLockedWidget() -> Bool {
        return false
    }
    
    func onKeyDownWhenLocked() -> BaseScreen {
        return self.screen
    }
    
    func onKeyUpWhenLocked() -> BaseScreen {
        return self.screen
    }
    
}
