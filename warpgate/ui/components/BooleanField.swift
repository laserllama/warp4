//
//  BooleanField.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//

import Foundation

class BooleanField : BaseField {
    
    var label: String
    var value: Bool
    var action: (_ newValue: Bool) -> Void
        
    init(screen: BaseScreen, label: String, value: Bool, action: @escaping (_ newValue: Bool) -> Void) {
        self.label = label
        self.value = value
        self.action = action
        super.init(screen: screen)
        self.hasValue = true
        self.valueDisplayWidth = 35
        
    }
    
    private func negateAndSet() -> BaseScreen {
        self.value = !(self.value)
        let _ = self.action(self.value)
        self.screen.drawFieldText(field: self)
        return self.screen
    }
            
    override func onLeft() -> BaseScreen {
        return self.negateAndSet()
    }
    
    override func onRight() -> BaseScreen {
        return self.negateAndSet()
    }
    
    override func getDisplayString() -> String {
        return self.label
    }
    
    override func getDisplayValue() -> String {
        return String(self.value)
    }
    
}
