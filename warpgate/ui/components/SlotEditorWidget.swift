//
//  SlotEditorWidget.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/8/21.
//

import Foundation

// FIXME: select based widgets don't render correctly - why?
// FIXME: copy and/or paste do not seem to work

// properties in WarpSlot that the UI knows how to edit:

enum SLOT_KEYS {
    case scaleNote
    case octaveShift
    case chordType
    case sharpFlat
    case tie
    case repeats
    case velocity
    case midiCCs_A
    case midiCCs_B
    case midiCCs_C
    case midiCCs_D
    case randomScaleNote
    case randomRepeats
    case randomVelocity
    case noteChance
    case tieChance
    case randomMidiCCs_A
    case randomMidiCCs_B
    case randomMidiCCs_C
    case randomMidiCCs_D
}

// how those properties are edited:

typealias SlotProperty = (name: String, property: SLOT_KEYS, min: Int, max: Int, choices: [String], forPattern: Bool, forArp: Bool)

let NO_STRINGS : [String] = []


var SLOT_PROPERTIES : [SlotProperty] = [
    (name: "note", property: SLOT_KEYS.scaleNote, min: -12, max: 12, choices: NO_STRINGS, forPattern: true, forArp: true),
    (name: "note!", property: SLOT_KEYS.scaleNote, min: 0, max: 10, choices: NO_STRINGS, forPattern: true, forArp: false),
    (name: "octave", property: SLOT_KEYS.octaveShift, min: -5, max: 5, choices: NO_STRINGS, forPattern: true, forArp: true),
    (name: "chord", property: SLOT_KEYS.chordType, min: 0, max: 1, choices: CHORD_TYPE_NAMES, forPattern: true, forArp: true),
    (name: "accidentals", property: SLOT_KEYS.sharpFlat, min: 0, max: 10, choices: SHARP_FLAT_NAMES, forPattern: true, forArp: true),
    (name: "mute/tie", property: SLOT_KEYS.tie, min: 0, max: 1, choices: TIE_TYPE_NAMES, forPattern: true, forArp: true),
    (name: "repeats", property: SLOT_KEYS.repeats, min: 0, max: 10, choices: NO_STRINGS, forPattern: true, forArp: true),
    (name: "velocity", property: SLOT_KEYS.velocity, min: 0, max: 120, choices: NO_STRINGS, forPattern: true, forArp: true),
    (name: "CC A", property: SLOT_KEYS.midiCCs_A, min: 0, max: 120, choices: NO_STRINGS, forPattern: true, forArp: false),
    (name: "CC B", property: SLOT_KEYS.midiCCs_B, min: 0, max: 120, choices: NO_STRINGS, forPattern: true, forArp: false),
    (name: "CC C", property: SLOT_KEYS.midiCCs_C, min: 0, max: 120, choices: NO_STRINGS, forPattern: true, forArp: false),
    (name: "CC D", property: SLOT_KEYS.midiCCs_D, min: 0, max: 120, choices: NO_STRINGS, forPattern: true, forArp: false),
    (name: "random note offset", property: SLOT_KEYS.randomScaleNote, min: 0, max: 20, choices: NO_STRINGS, forPattern: true, forArp: true),
    (name: "random tie chance", property: SLOT_KEYS.tieChance, min: 0, max: 100, choices: NO_STRINGS, forPattern: true, forArp: true),
    (name: "random repeats", property: SLOT_KEYS.randomRepeats, min: 0, max: 10, choices: NO_STRINGS, forPattern: true, forArp: true),
    (name: "random note chance", property: SLOT_KEYS.noteChance, min: 0, max: 100, choices: NO_STRINGS, forPattern: true, forArp: true),
    (name: "random velocity offset", property: SLOT_KEYS.randomVelocity, min: 0, max: 120, choices: NO_STRINGS, forPattern: true, forArp: true),
    (name: "random CC A offset", property: SLOT_KEYS.randomMidiCCs_A, min: 0, max: 120, choices: NO_STRINGS, forPattern: true, forArp: true),
    (name: "random CC B offset", property: SLOT_KEYS.randomMidiCCs_B, min: 0, max: 120, choices: NO_STRINGS, forPattern: true, forArp: true),
    (name: "random CC C offset", property: SLOT_KEYS.randomMidiCCs_C, min: 0, max: 120, choices: NO_STRINGS, forPattern: true, forArp: true),
    (name: "random CC D offset", property: SLOT_KEYS.randomMidiCCs_D, min: 0, max: 120, choices: NO_STRINGS, forPattern: true, forArp: true),
]

// names of all the properties we can edit, so we can page between them:

var SLOT_PROPERTY_NAMES_PATTERN : [String] = SLOT_PROPERTIES.filter { $0.forPattern == true } . map { $0.name }

var SLOT_PROPERTY_NAMES_ARP : [String] = SLOT_PROPERTIES.filter { $0.forArp == true } . map { $0.name }


class SlotEditorWidget<ValueType : HasSlots> : BaseField {
    
    // key inputs
    var song : WarpSong
    var container : ValueType
    
    // what property we are currently editing
    var property : String = "note"
    
    // a locked widget is active and intercepts key up/down events versus letting them go to the screen
    var locked : Bool = false
    
    // buffer for copy/paste
    var copySlots : [WarpSlot] = []
    
    // UI formatting details
    let leftIndent : Int = 10    // where to start drawing the widget
    let pages: Int = 4           // how many pages of slots
    let rowsPerPage: Int = 16    // how many slots to draw on each page
    let cellWidth: Int = 6       // how wide each cell is when drawing
    let meterHeight: Int = 12    // how many vertical rows for the slot editor widgets adjustable section
    let maxSlots: Int = 64
    
    // remembering the current selection
    
    var selectedPage = 0             // start out with the very first page & slot selected
    var selectedSlot = 0             // ...
    var selectedSlots : [Int] = []   // allows for multiple selections
    
    // whether we are multi-selecting
    
    var gangMode : Bool = false
    
    let blankCell = "|     "               // how to draw a blank cell
    let editCell = "EDIT"                  // how to draw an 'active' cell
        
    // slightly different behavior for arps
    let forPattern : Bool
    
    var slotPropertyNames : [String] = []
    //var firstSlotProperty : String
    
    init(screen: BaseScreen, song: WarpSong, container: ValueType, property: String, forPattern: Bool) {
        self.song = song
        self.container = container
        self.property = property
        self.forPattern = forPattern
        if (forPattern) {
            self.slotPropertyNames = SLOT_PROPERTY_NAMES_PATTERN
        } else {
            self.slotPropertyNames = SLOT_PROPERTY_NAMES_ARP
        }
        
        if (container.isPercussion()) {
            self.property = "note!"
        }
        super.init(screen: screen)
    }
        
    override func getDisplayString() -> String {
        // not used, because of the custom renderer
        return ""
    }
    
    override func getDisplayHeight() -> Int {
        // how large is the widget
        return 12 + self.meterHeight
    }
    
    override func isLockedWidget() -> Bool {
        // the widget is locked when manuevering inside, which means it gets most events and not the screen
        return self.locked
    }
        
    override func onEnter() -> BaseScreen {
        // enter doesn't do anything
        return self.screen
    }
    
    override func onKeyUpWhenLocked() -> BaseScreen {
        // move all selected fields up, if possible
        self.incrementSelectedSlots()
        return self.screen
    }
    
    override func onKeyDownWhenLocked() -> BaseScreen {
        // move all selected fields down if possible
        self.decrementSelectedSlots()
        return self.screen
    }
        
    func onPeriod() -> Void {
        self.gangMode = true
        if !self.selectedSlots.contains(where: { $0 == self.selectedSlot }) {
            // add the slot to the selection
            self.selectedSlots.append(self.selectedSlot)
        }
        else {
            // already selected, ungang
            self.selectedSlots = self.selectedSlots.filter({ $0 != self.selectedSlot })
        }
        redraw()
    }
    
    func onSlash() -> Void {
        // remove all slots from the selection
        self.gangMode = false
        self.selectedSlots = [ self.selectedSlot ]
        redraw()
    }
    
    override func onOtherKey(key: Int32) -> BaseScreen {
        if (key == 9) { // tab
            self.locked = !(self.locked)
            redraw()
        }
        if (key == 46) { // period
            self.onPeriod()
        }
        else if (key == 47) { // forward slash
            self.onSlash()
        }
        else if (key == 339) { // page up
            self.property = self.getPreviousProperty()
            redraw()
            
        }
        else if (key == 338) { // page down
            self.property = self.getNextProperty()
            redraw()
        }
        WarpLogger.log(message: "key: \(key)")
        return self.screen
    }
    
    func onCopy() -> Void {
        // 'c' for copy -- FIXME, doesn't quite work
        self.copySlots = []
        if (self.gangMode) {
            
            for slotPosition in self.selectedSlots {
                //WarpLogger.log(message: "COPY: gang mode, adding slots = \(slotPosition)")
                self.copySlots.append(self.container.getSlot(slotPosition))
            }
        } else {
            //WarpLogger.log(message: "COPY: not in gang mode, copyslots = \(self.selectedSlots)")
            self.copySlots.append(self.container.getSlot(self.selectedSlot))
        }
        redraw()
    }
    
    func onPaste() -> Void {
        // 'p' for paste -- FIXME< doesn't quite work

        var offset = self.selectedSlot

        for copySlot in self.copySlots {
            if (self.selectedSlot + offset <= 63) {
                //WarpLogger.log(message: "PASTE, SS is \(self.selectedSlot), offset is = \(offset)")
                self.container.setSlot(self.selectedSlot + offset, copySlot.copy())
            }
            offset = offset + 1
        }
        redraw()
    }
    
    func onSelectCurrentPage() -> Void {
        // 'a' for select  current page
        
        self.selectedSlots = []
        let start = self.getStartDisplaySlot()
        let end = self.getEndDisplaySlot()
        for slotPosition in 0...63 {
            if (slotPosition >= start) && (slotPosition <= end) {
                self.selectedSlots.append(slotPosition)
            }
        }
        redraw()
    }
    
    override func onRegularCharacter(key: Int32) -> BaseScreen {
                
        if (key == 99) {
            // 'c'
            self.onCopy()
        }
                
        else if (key == 112) {
            // 'p'
            self.onPaste()
        }
        
        else if (key == 97) {
            // 'a'
            self.onSelectCurrentPage()
        }
        
        return self.screen
        
    }
    
    override func getCustomDisplayHandler() -> ((_ screenRow: Int) -> Void)? {
        // part of the base class signature, this allows overriding the normal widget display system baked into BaseScreen
        // and allows us to draw a multi-line widget instead
        return self.widgetRenderer
    }
    
    @inline(__always) func drawCapRow(screenRow: Int) {
        // draw a vertical line above or below the table
        if (self.locked) {
            self.screen.print(row: screenRow, col: self.leftIndent, text: "+=====+=====+=====+=====+=====+=====+=====+=====+=====+=====+=====+=====+=====+=====+=====+=====")

        } else {
            self.screen.print(row: screenRow, col: self.leftIndent, text: "+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----")
        }
    }
    
    @inline(__always) func getStartDisplaySlot() -> Int {
        // what is the number of the first slot to show in the table based on the current selection?
        return self.selectedPage * 16
    }
    
    @inline(__always) func getEndDisplaySlot() -> Int {
        // what is the last slot to show in the table?
        return (self.selectedPage * 16) + 15
    }
        
    @inline(__always) func getSlotAtPosition(slotPosition: Int) -> WarpSlot {
        // return the slot object for a given selection number
        return self.container.getSlot(slotPosition)
    }
    
    @inline(__always) func getSlotProperty(name: String) -> SlotProperty {
        // for a slot property like "octaveShift", return the UI definition/rules, aka the SlotProperty object
        return SLOT_PROPERTIES.first(where: { $0.name == name})!
    }
    
    func getSlotHeight(slotPosition: Int) -> Int {
                
        // all values are displayed in a vertical meter - the height of which can be customized
        // for each slot (0-63), the value is drawn in one of those (usually 1 to 10) vertical points,
        // but not all of them. This means higher values can be drawn higher, for most things, though
        // for some items from a list, the logic is more specific.
        
        let slotProperty: SlotProperty = getSlotProperty(name: self.property)
        let slot = self.getSlotAtPosition(slotPosition: slotPosition)
                
        switch slotProperty.property {
        case SLOT_KEYS.scaleNote:
            return calculateSelectionHeight(value: slot.scaleNote, property: slotProperty)
        case SLOT_KEYS.octaveShift:
            return calculateSelectionHeight(value: slot.octaveShift, property: slotProperty)
        case SLOT_KEYS.chordType:
            return (slot.chordType == ChordType.off) ? 2 : 8
        case SLOT_KEYS.sharpFlat:
            if (slot.sharpFlat == SharpFlat.SHARP) {
                return 8
            }
            else if (slot.sharpFlat == SharpFlat.FLAT) {
                return 5
            } else {
                return 2
            }
        case SLOT_KEYS.tie:
            if (slot.tie == TieType.Off) {
                return 2
            }
            else {
                return 8
            }
        case SLOT_KEYS.repeats:
            return calculateSelectionHeight(value: slot.repeats, property: slotProperty)
        case SLOT_KEYS.velocity:
            return calculateSelectionHeight(value: slot.velocity, property: slotProperty)
        case SLOT_KEYS.midiCCs_A:
            return calculateSelectionHeight(value: slot.midiCCs[0], property: slotProperty)
        case SLOT_KEYS.midiCCs_B:
            return calculateSelectionHeight(value: slot.midiCCs[1], property: slotProperty)
        case SLOT_KEYS.midiCCs_C:
            return calculateSelectionHeight(value: slot.midiCCs[2], property: slotProperty)
        case SLOT_KEYS.midiCCs_D:
            return calculateSelectionHeight(value: slot.midiCCs[3], property: slotProperty)
        case SLOT_KEYS.randomScaleNote:
            return calculateSelectionHeight(value: slot.randomScaleNote, property: slotProperty)
        case SLOT_KEYS.randomRepeats:
            return calculateSelectionHeight(value: slot.randomRepeats, property: slotProperty)
        case SLOT_KEYS.randomVelocity:
            return calculateSelectionHeight(value: slot.randomVelocity, property: slotProperty)
        case SLOT_KEYS.noteChance:
            return calculateSelectionHeight(value: slot.noteChance, property: slotProperty)
        case SLOT_KEYS.tieChance:
            return calculateSelectionHeight(value: slot.tieChance, property: slotProperty)
        case SLOT_KEYS.randomMidiCCs_A:
            return calculateSelectionHeight(value: slot.randomMidiCCs[0], property: slotProperty)
        case SLOT_KEYS.randomMidiCCs_B:
            return calculateSelectionHeight(value: slot.randomMidiCCs[1], property: slotProperty)
        case SLOT_KEYS.randomMidiCCs_C:
            return calculateSelectionHeight(value: slot.randomMidiCCs[2], property: slotProperty)
        case SLOT_KEYS.randomMidiCCs_D:
            return calculateSelectionHeight(value: slot.randomMidiCCs[3], property: slotProperty)
        }

    }
    
    func calculateSelectionHeight(value: Int, property: SlotProperty) -> Int {
        // helper function for getSlotHeight - for integer fields, the draw height is based on a percentage of the value between min and max
        let range = Double(property.max - property.min)
        let offset = Double(property.max - value)
        let fraction : Double = offset/range
        return self.meterHeight - Int(fraction * Double(self.meterHeight))
    }
    
    func tweakInt(value: inout Int, slotProperty: SlotProperty, amount: Int) -> Void {
        // this method is used to increment or decrement a selected field with an integer value
        // but it will not move it outside the min/max range for that property
        
        if (value + amount > slotProperty.max) {
            return
        }
        if (value + amount < slotProperty.min) {
            return
        }
        value += amount
    }
    
    func rotate(value: String, slotProperty: SlotProperty, amount: Int) -> String {
        
        // for fields that are not  integer related - usually enums - we cycle forward
        // or back through all the possible values.
        
        let choices = slotProperty.choices
        let currentIndex = choices.firstIndex(where: { $0 == value })!
        
        if (amount < 0) {
            if (currentIndex > 0) {
                return choices[currentIndex - 1]
            }
        }
        else {
            if (currentIndex != choices.count - 1) {
                return choices[currentIndex + 1]
            }
        }
        return choices[currentIndex]
    }
        
    func modify(slotPosition: Int, amount: Int) -> Void {
        
        // this function increments or decrements a specific slot, though the behavior varies based on what property of the slot
        // we are modifying.  Uses the above functions 'rotate' (for enums) and 'tweakInt' (for ints).
        
        let slotProperty: SlotProperty = getSlotProperty(name: self.property)
        let slot = self.getSlotAtPosition(slotPosition: slotPosition)
            
        switch slotProperty.property {
            case SLOT_KEYS.scaleNote:
                tweakInt(value: &slot.scaleNote, slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.octaveShift:
                tweakInt(value: &slot.octaveShift, slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.chordType:
                slot.chordType = ChordType(rawValue: rotate(value: slot.chordType.rawValue, slotProperty: slotProperty, amount: amount))!
            case SLOT_KEYS.sharpFlat:
                slot.sharpFlat = SharpFlat(rawValue: rotate(value: slot.sharpFlat.rawValue, slotProperty: slotProperty, amount: amount))!
            case SLOT_KEYS.tie:
                slot.tie = TieType(rawValue: rotate(value: slot.tie.rawValue, slotProperty: slotProperty, amount: amount))!
            case SLOT_KEYS.repeats:
                tweakInt(value: &slot.repeats, slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.velocity:
                tweakInt(value: &slot.velocity, slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.midiCCs_A:
                tweakInt(value: &slot.midiCCs[0], slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.midiCCs_B:
                tweakInt(value: &slot.midiCCs[1], slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.midiCCs_C:
                tweakInt(value: &slot.midiCCs[2], slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.midiCCs_D:
                tweakInt(value: &slot.midiCCs[3], slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.randomScaleNote:
                tweakInt(value: &slot.randomScaleNote, slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.randomRepeats:
                tweakInt(value: &slot.randomRepeats, slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.randomVelocity:
                tweakInt(value: &slot.randomVelocity, slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.noteChance:
                tweakInt(value: &slot.noteChance, slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.tieChance:
                tweakInt(value: &slot.tieChance, slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.randomMidiCCs_A:
                tweakInt(value: &slot.randomMidiCCs[0], slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.randomMidiCCs_B:
                tweakInt(value: &slot.randomMidiCCs[1], slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.randomMidiCCs_C:
                tweakInt(value: &slot.randomMidiCCs[2], slotProperty: slotProperty, amount: amount)
            case SLOT_KEYS.randomMidiCCs_D:
                tweakInt(value: &slot.randomMidiCCs[3], slotProperty: slotProperty, amount: amount)
            }
    }
        
    func getCellDisplayText(slotPosition: Int) -> String {
        // inside each vertical meter we'll draw the actual value of the slot's property, which is
        // usually the string version of the enum or the integer value.
                    
        let slotProperty: SlotProperty = getSlotProperty(name: self.property)
        let slot = self.getSlotAtPosition(slotPosition: slotPosition)
                
        switch slotProperty.property {
            case SLOT_KEYS.scaleNote:
                if (slot.scaleNote == 0) {
                    return "-"
                }
                return String(slot.scaleNote)
            case SLOT_KEYS.octaveShift:
                return String(slot.octaveShift)
            case SLOT_KEYS.chordType:
                return slot.chordType.rawValue
            case SLOT_KEYS.sharpFlat:
                return slot.sharpFlat.rawValue
            case SLOT_KEYS.tie:
                return slot.tie.rawValue
            case SLOT_KEYS.repeats:
                if (slot.repeats == 0) {
                    return "none"
                }
                return String(slot.repeats)
            case SLOT_KEYS.velocity:
                return String(slot.velocity)
            case SLOT_KEYS.midiCCs_A:
                return String(slot.midiCCs[0])
            case SLOT_KEYS.midiCCs_B:
                return String(slot.midiCCs[1])
            case SLOT_KEYS.midiCCs_C:
                return String(slot.midiCCs[2])
            case SLOT_KEYS.midiCCs_D:
                return String(slot.midiCCs[3])
            case SLOT_KEYS.randomScaleNote:
                return String(slot.randomScaleNote)
            case SLOT_KEYS.randomRepeats:
                return String(slot.randomRepeats)
            case SLOT_KEYS.randomVelocity:
                return String(slot.randomVelocity)
            case SLOT_KEYS.noteChance:
                return String(slot.noteChance)
            case SLOT_KEYS.tieChance:
                return String(slot.tieChance)
            case SLOT_KEYS.randomMidiCCs_A:
                return String(slot.randomMidiCCs[0])
            case SLOT_KEYS.randomMidiCCs_B:
                return String(slot.randomMidiCCs[1])
            case SLOT_KEYS.randomMidiCCs_C:
                return String(slot.randomMidiCCs[2])
            case SLOT_KEYS.randomMidiCCs_D:
                return String(slot.randomMidiCCs[3])
            }
    }
    
    func decrementSelectedSlots() -> Void {
        // used in the down arrow handler, this moves all selected slots down in value
        if (self.gangMode) {
            for slotNumber in self.selectedSlots {
                self.modify(slotPosition: slotNumber, amount: -1)
            }
        } else {
            self.modify(slotPosition: self.selectedSlot, amount: -1)
        }
        redraw()
    }
    
    func incrementSelectedSlots() -> Void {
        // used in the up arrow handler, this moves all selected slots up
        if (self.gangMode) {
            for slotNumber in self.selectedSlots {
                self.modify(slotPosition: slotNumber, amount: 1)
            }
        } else {
            self.modify(slotPosition: self.selectedSlot, amount: 1)
        }
        redraw()
    }
    
    func getNextProperty() -> String {
        let index : Int = self.slotPropertyNames.firstIndex(where: { $0 == self.property })!
        if index + 1 < self.slotPropertyNames.count {
            return self.slotPropertyNames[index + 1]
        } else {
            return self.slotPropertyNames.last!
        }
    }
    
    func getPreviousProperty() -> String {
        let index : Int = self.slotPropertyNames.firstIndex(where: { $0 == self.property })!
        if index > 0 {
            return self.slotPropertyNames[index - 1]
        } else {
            return self.slotPropertyNames.first!
        }
    }
    
    override func onLeft() -> BaseScreen {
        // left arrow is hit
                    
        // moving the cursor left stops at 0, and going past 0 unlocks the widget
        if (self.locked) {
            if (self.selectedSlot > 0) {
                self.selectedSlot = self.selectedSlot - 1
            }
        } else {
            self.property = self.getPreviousProperty()
        }
        
        self.selectedPage = ( self.selectedSlot / 16) // FIXME: *BROKEN* ... we can probably remove this variable and just behave as needed.
    
        redraw()
        
        if (self.property == "note!") && (!(self.container.isPercussion())) {
            self.property = "note"
        }
        if (self.property == "note") && (self.container.isPercussion()) {
            self.property = "note!"
        }
        
        return self.screen
    }
    
    override func onRight() -> BaseScreen {
        // moving the cursor right locks the widget if not already locked, and will stop when getting to the maximum slot (63)
        
        if  (self.locked) {
            if (self.selectedSlot < self.maxSlots - 1) {
                self.selectedSlot = self.selectedSlot + 1
            }
            // select the current slot we end up at ...
            self.selectedPage = ( self.selectedSlot / 16)
        } else {
            self.property = self.getNextProperty()

        }
        
        if (self.property == "note!") && (!(self.container.isPercussion())) {
            self.property = "note"
        }
        if (self.property == "note") && (self.container.isPercussion()) {
            self.property = "note!"
        }

        redraw()
        return self.screen
    }
    
    /*
     
    NOTES / FIXME: work on moving a selection wasn't completed, but we'd need a "move" mode - which might be confusing.
    it's probably better not to have it.
     
    it might be nicer to have a 'reset' option instead so copy/paste could happen and then all the
    selected options could be reset with something like 'r'
     
    func moveSelectionLeft() {
        copySlots = self.pattern.slots.map { $0.copy() }
        for selection in self.selectedSlots {
            if (selection != 0) {
                copySlots[selection - 1] = self.pattern.slots[selection].copy()
            }
        }
        self.selectedSlots = self.selectedSlots.map { $0 - 1 }.filter { $0 > 0 }

        self.pattern.slots = copySlots
        redraw()
    }
    
    func moveSelectionRight() {
        copySlots = self.pattern.slots.map { $0.copy() }
        for selection in self.selectedSlots {
            if (selection != 0) {
                copySlots[selection - 1] = self.pattern.slots[selection].copy()
            }
        }
        self.selectedSlots = self.selectedSlots.map { $0 + 1 }.filter { $0 < 63}
        
        self.pattern.slots = copySlots
        redraw()
        
    }
    */
    
    
    func drawSlotNumbers(screenRow: Int) {
        
        // drawing the widget has several components
        // this function draws the captions that show what slot number is what across the top
    
        let startIndex : Int = self.getStartDisplaySlot()
        let endIndex : Int = self.getEndDisplaySlot()
        var selectionIndicatorOffset : Int = 1
    
        for slotPosition in startIndex ... endIndex {
    
            let startColumn : Int = self.leftIndent + (self.cellWidth * (slotPosition - startIndex))
            let numberStart : Int = startColumn + 2
    
            self.screen.print(row: screenRow, col: startColumn, text: self.blankCell)
            self.screen.print(row: screenRow, col: startColumn+2, text: String(slotPosition))

            if (slotPosition > 9) {
                selectionIndicatorOffset = 2
            }
            
            if (slotPosition == self.selectedSlot) {
                self.screen.print(row: screenRow, col: numberStart + selectionIndicatorOffset, text: "*")
            } else if (self.selectedSlots.contains(where: { $0 == slotPosition })) {
                self.screen.print(row: screenRow, col: numberStart + selectionIndicatorOffset, text: "!")
            }
    
        }
    }

    func drawPropertyValues(screenRow: Int, yHeight: Int) {
        
        // this function draws a vertical slice of the meters section, whether or not we draw a value
        // in each cell depends on whether or not the slot's property's value thinks it is a good
        // place to draw it.  This is the "meters" concept. See 'getSlotHeight' for details.
        
        let startIndex : Int = self.getStartDisplaySlot()
        let endIndex : Int = self.getEndDisplaySlot()
        
        for slotPosition in startIndex ... endIndex {
            let startColumn : Int = self.leftIndent + (self.cellWidth * (slotPosition - startIndex))
            let valueStart : Int = startColumn + 2
            
            let requestedHeight : Int = self.getSlotHeight(slotPosition: slotPosition)
            self.screen.print(row: screenRow, col: startColumn, text: self.blankCell)
            if (yHeight == requestedHeight) {
                let cellDisplayText : String = getCellDisplayText(slotPosition: slotPosition)
                self.screen.print(row: screenRow, col: valueStart, text: cellDisplayText)
            } else {
                self.screen.print(row: screenRow, col: valueStart, text: "     ")
            }
        }
    }
        
    func widgetRenderer(screenRow: Int) -> Void {
        
        // this draws the entire widget, at the location starting where the BaseScreen says to draw it
        // it also extends several rows after because this is a multi-line widget.
        
        if (!(self.locked)) {
            // FIXME: all the display width logic in the various fields should go away, along with this spacing, so we can just
            // call individual functions in the uiRenderer and everything is aligned correctly.
            self.screen.print(row: screenRow, col: 3, text: "Editor                     <                                  >")
        } else {
            // FIXME: see previous comment
            self.screen.print(row: screenRow, col: 3, text: "Editor                     <                                  >")
        }
        self.screen.print(row: screenRow, col: 32, text: property)
        self.screen.print(row: screenRow + 1, col: self.leftIndent, text: "                 ")

        self.drawCapRow(screenRow: screenRow + 2)
        self.drawSlotNumbers(screenRow: screenRow + 3)
        self.drawCapRow(screenRow: screenRow + 4)
        
        self.drawPropertyValues(screenRow: screenRow + 5, yHeight: 12)
        self.drawPropertyValues(screenRow: screenRow + 6, yHeight: 11)
        self.drawPropertyValues(screenRow: screenRow + 7, yHeight: 10)
        self.drawPropertyValues(screenRow: screenRow + 8, yHeight: 9)
        self.drawPropertyValues(screenRow: screenRow + 9, yHeight: 8)
        self.drawPropertyValues(screenRow: screenRow + 10, yHeight: 7)
        self.drawPropertyValues(screenRow: screenRow + 11, yHeight: 6)
        self.drawPropertyValues(screenRow: screenRow + 12, yHeight: 5)
        self.drawPropertyValues(screenRow: screenRow + 13, yHeight: 4)
        self.drawPropertyValues(screenRow: screenRow + 14, yHeight: 3)
        self.drawPropertyValues(screenRow: screenRow + 15, yHeight: 2)
        self.drawPropertyValues(screenRow: screenRow + 16, yHeight: 1)
        self.drawPropertyValues(screenRow: screenRow + 17, yHeight: 0)
        self.drawCapRow(screenRow: screenRow + 18)

        self.screen.print(row: screenRow + 20, col: 20, text: "{ TAB focus       | left/right navigate | up/down adjust |                      } ")
        self.screen.print(row: screenRow + 21, col: 20, text: "{ . multi select  | / unselect          | c/p copy/paste | PAGE UP/DN chg-field } ")
            
    }
    
    @inline(__always) func redraw() -> Void {
        // just re-render the widget
        widgetRenderer(screenRow: self.screenRow)
    }
    
}
