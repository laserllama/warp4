//
//  ActionField.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//

import Foundation

// FIXME: this should maybe be entirely replaced with ButtonBar.

class ActionField : BaseField {
    
    var label: String
    var reloadOnEnter: Bool
    var action: () -> BaseScreen
    
    init(screen: BaseScreen, label: String, reloadOnEnter: Bool, action: @escaping () -> BaseScreen) {
        self.label = label
        self.reloadOnEnter = reloadOnEnter
        self.action = action
        super.init(screen: screen)
    }
    
    override func shouldReloadOnEnter() -> Bool {
        return self.reloadOnEnter
    }
        
    override func onEnter() -> BaseScreen {
        return self.action()
    }
    
    override func getDisplayString() -> String {
        return self.label
    }
    
}
