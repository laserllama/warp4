//
//  StringField.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/4/21.
//


import Foundation

class StringField : BaseField {
    
    var label: String
        
    init(screen: BaseScreen, label: String) {
        self.label = label
        super.init(screen: screen)
    }
    
    override func isSelectable() -> Bool {
        return false
    }
        
    override func getDisplayString() -> String {
        return self.label
    }
    
}
