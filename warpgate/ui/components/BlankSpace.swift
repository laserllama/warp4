//
//  BlankSpace.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//

import Foundation

class BlankSpace : BaseField {
                
    override func getDisplayString() -> String {
        return ""
    }
    
    override func isSelectable() -> Bool {
        return false
    }
    
}
