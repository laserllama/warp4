//
//  BooleanField.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//

// WARNING: this class must only be used when there are *SOME* entries in 'choices'

import Foundation

class SelectField<ValueType> : BaseField {
    
    var label: String
    var value: ValueType
        
    var choices: [ValueType]
    var toString: (ValueType) -> String
    var action: (_ newValue: ValueType) -> Void
    var rotate: Bool
        
    init(screen: BaseScreen, label: String, value: ValueType, choices: [ValueType], toString: @escaping (ValueType) -> String, rotate: Bool = false, action: @escaping (_ newValue: ValueType) -> Void) {
        self.label = label
        self.choices = choices
        self.action = action
        self.value = value
        self.toString = toString
        self.rotate = rotate
        super.init(screen: screen)
        self.hasValue = true
        self.valueDisplayWidth = 35
    }
     
    func findSelectedKey(value: ValueType) -> String {
        for item in self.choices {
            if (self.toString(item) == self.toString(value)) { return self.toString(item) }
        }
        return self.toString(self.choices.first!)
    }
    
    /*
    func findSelectedItem(String: needle) -> ValueType? {
        for item in self.choices {
            if (self.toString(item) == needle) {
                return item
            }
        }
        return nil
    }
    */
    
    func findNextSelection() -> ValueType {
        var found = false
        let selectedKey = self.findSelectedKey(value: self.value)

        for item in self.choices {
            if (found) { return item }
            if (selectedKey == self.toString(item)) { found = true }
        }
        if (rotate) {
            return self.choices.first!
        } else {
            return self.choices.last!
        }
    }
    
    func findPreviousSelection() -> ValueType {
        var found = false
        let selectedKey = self.findSelectedKey(value: self.value)

        for item in self.choices.reversed() {
            if (found) { return item }
            if (selectedKey == self.toString(item)) { found = true }
        }
        if (rotate) {
            return self.choices.last!
        }
        else {
            return self.choices.first!
        }
    }
    
    override func onLeft() -> BaseScreen {
        self.value = self.findPreviousSelection()
        self.action(self.value)
        self.screen.drawFieldText(field: self)
        return self.screen
    }
    
    override func onRight() -> BaseScreen {
        self.value = self.findNextSelection()
        self.action(self.value)
        self.screen.drawFieldText(field: self)
        return self.screen
    }
    
    override func getDisplayString() -> String {
        return self.label
    }
    
    override func getDisplayValue() -> String {
        return self.findSelectedKey(value: self.value)
    }
    
}

