//
//  BrowserWidget.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/12/21.
//

import Foundation

// POSSIBLY DEFUNCT...
// FIXME: a prototype for the SongGrid .. its not very smart yet, when it gets written for tracks/songs it may be better

class BrowserWidget<ValueType : Browseable> : BaseField {
    
    var collection : [ValueType]
    
    var displayFunc: (ValueType) -> String
    var addFunc: () -> BaseScreen
    var openFunc: (ValueType) -> BaseScreen
    
    var actualRowCursor: Int = 0
    var actualColCursor: Int = 0
    var viewportRowCount: Int = 0
    var viewportColumnCount: Int = 0
    
    var locked : Bool = true
    
    let leftOffset : Int = 10
    //let linesPerScreen : Int = 10
    //let itemsPerRow : Int = 3
    let fieldSize: Int = 20
    //let fieldSpacing: Int = 5
    let headerSize : Int = 1
    
    init(screen: BaseScreen,
         viewportRowCount: Int,
         viewportColumnCount: Int,
         collection: [ValueType],
         displayFunc: @escaping (ValueType) -> String,
         openFunc: @escaping (ValueType) -> BaseScreen,
         addFunc: @escaping () -> BaseScreen
    ) {
        self.viewportRowCount = viewportRowCount
        self.viewportColumnCount = viewportColumnCount
        self.collection = collection
        self.displayFunc = displayFunc
        self.openFunc = openFunc
        self.addFunc = addFunc
        super.init(screen: screen)
    }
    
    override func onRegularCharacter(key: Int32) -> BaseScreen {
        if (key == 110) {
            return self.addFunc() 
        }
        return self.screen
    }
        
    func getRealCursorInformation(horizontalPage: Int, verticalPage: Int, virtualCol: Int, virtualRow: Int) -> (Int, Int) {
        let realCol : Int = Int(horizontalPage * self.viewportColumnCount) + virtualCol
        let realRow : Int = Int(verticalPage * self.viewportRowCount) + virtualRow
        return (realCol, realRow)
    }

    func getVirtualCursorInformation() -> (Int, Int) {
        let verticalPage : Int = self.getVerticalPage()
        let virtualRow : Int = self.actualRowCursor - (verticalPage * self.viewportRowCount)
        let virtualCol : Int = self.actualColCursor
        WarpLogger.log(message: "\(self.actualColCursor) \(self.actualRowCursor) is \(virtualCol) \(virtualRow) vp=\(verticalPage)")
        return (virtualCol, virtualRow)
    }
    
    func getVerticalPage() -> Int {
        return (self.actualRowCursor / self.viewportRowCount)
    }
    
    func getHorizontalPage() -> Int {
        return 0
    }

    override func getDisplayString() -> String {
        // not used, because of the custom renderer
        return ""
    }
    
    override func getDisplayHeight() -> Int {
        // how large is the widget
        return self.viewportColumnCount + 3
    }
    
    override func isLockedWidget() -> Bool {
        // the widget is locked when manuevering inside, which means it gets most events and not the screen
        return self.locked
    }
        
    override func onEnter() -> BaseScreen {
        // if we have something selected, call addFunc or addFunc + openFunc
        let obj : ValueType? = getObjectAtCell(col: self.actualColCursor, row: self.actualRowCursor)
        if (obj != nil) {
            return self.openFunc(obj!)
        }
        return self.screen
    }
    
    func getObjectAtCell(col: Int, row: Int) -> ValueType? {
        // the base class provides viewport horizontal scrolling if subclassed
        let index = (row * self.viewportRowCount) + col
        WarpLogger.log(message: "looking up \(col) and \(row) with item count = \(self.collection.count) and index = \(index)")
        if (index <= self.collection.count - 1) {
            //WarpLogger.log(message: "found something at this index: \(index)")
            let obj : ValueType = self.collection[index]
            //WarpLogger.log(message: "named \(obj.getName())")
            return obj
        }
        return nil
    }
        
    override func getCustomDisplayHandler() -> ((_ screenRow: Int) -> Void)? {
        // part of the base class signature, this allows overriding the normal widget display system baked into BaseScreen
        // and allows us to draw a multi-line widget instead
        return self.widgetRenderer
    }
  
    override func onLeft() -> BaseScreen {
        if (self.actualColCursor > 0) {
            self.actualColCursor -= 1
        }
        redraw()
        return self.screen
    }
    
    override func onRight() -> BaseScreen {
        if (self.actualColCursor < self.viewportColumnCount - 1) {
            self.actualColCursor += 1
        }
        redraw()
        return self.screen
    }
    
    @inline(__always) func redraw() -> Void {
        // just re-render the widget
        widgetRenderer(self.screenRow)
    }
    
    override func onKeyUpWhenLocked() -> BaseScreen {
        if (self.actualRowCursor > 0) {
            self.actualRowCursor -= 1
        }
        redraw()
        return self.screen
    }
        
    
    override func onKeyDownWhenLocked() -> BaseScreen {
        //let linesRequired = self.collection.count / self.viewportRowCount
        if (self.actualRowCursor < self.viewportRowCount - 1) {
            self.actualRowCursor += 1
        }
        redraw()
        return self.screen
    }

    func drawCell(col: Int, row: Int, text: String) {
        // inputs are virtual coordinates
        WarpLogger.log(message: "drawing \(text)")
        self.screen.print(row: self.headerSize + self.screenRow + row, col: 2 + self.leftOffset + (col * (self.fieldSize)) + 2, text: text)
    }
    
    func drawCursor(col: Int, row: Int, active: Bool) {
        // inputs are virtual coordinates
        
        var leftText = " "
        if (active) {
            leftText = "*"
        }
        self.screen.print(row: self.headerSize + self.screenRow + row, col: self.leftOffset + (col * (self.fieldSize)) + 1, text: leftText)
    }

    func widgetRenderer(_ screenRow: Int) {
        
        // start drawing the table at relative coordinates (0,0)
        
        //let filler : String = "                                          "
        var viewportRow : Int = 0
        var viewportCol : Int = 0
        
        self.screen.print(row: self.screenRow, col: self.leftOffset, text: "+----------------------------------------------------------------------------------")
                
        for i in 0 ... self.viewportRowCount - 1 {
            for j in 0 ... self.viewportColumnCount - 1{
                
                self.screen.print(row: self.screenRow + i + 1, col: self.leftOffset, text: "|")
                
                viewportRow = i
                viewportCol = j
                
                self.drawCursor(col: viewportCol, row: viewportRow, active: false)
                let horizontalPage : Int = self.getHorizontalPage()
                let verticalPage : Int = self.getVerticalPage()
                let (realCol, realRow) : (Int, Int) = getRealCursorInformation(horizontalPage: horizontalPage, verticalPage: verticalPage, virtualCol: viewportCol, virtualRow: viewportRow)
                let obj : ValueType? = getObjectAtCell(col: realCol, row: realRow)
                
                if (obj != nil) {
                    self.drawCell(col: viewportCol, row: viewportRow, text: displayFunc(obj!))
                }
            }
        }
        
        let (cursorCol, cursorRow) : (Int, Int) = getVirtualCursorInformation()
        self.drawCursor(col: cursorCol, row: cursorRow, active: true)
        self.screen.print(row: self.screenRow + self.viewportRowCount + 2 , col: self.leftOffset, text: "                  { arrows: select     n: new }")

    }
    

}


