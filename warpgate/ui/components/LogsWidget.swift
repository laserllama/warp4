//
//  LogsWidget.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/11/21.
//

import Foundation


class LogsWidget : BaseField {
        
    // a locked widget is active and intercepts key up/down events versus letting them go to the screen
    var locked : Bool = false
    var cursor: Int = -1
    let messagesPerScreen : Int = 15
    var messages : [String] = []
    
    override init(screen: BaseScreen) {
        super.init(screen: screen)
        reload()
    }
        
    override func getDisplayString() -> String {
        // not used, because of the custom renderer
        return ""
    }
    
    func reload() {
        self.messages = WarpLogger.getMessages()
        if (self.cursor == -1) {
            self.cursor  = self.messages.count - self.messagesPerScreen
        }
    }
    
    override func getDisplayHeight() -> Int {
        // how large is the widget
        return self.messagesPerScreen + 1
    }
    
    override func onKeyUpWhenLocked() -> BaseScreen {
        if (self.cursor < 0) {
            self.cursor = 0
        }
        if (self.cursor > 0) {
            self.cursor -= 1
        }
        redraw()
        return self.screen
    }
        
    
    override func onKeyDownWhenLocked() -> BaseScreen {
        if ((self.cursor + self.messagesPerScreen) < self.messages.count - 1) {
            self.cursor += 1
            redraw()
        }
        //WarpLogger.log(message: "new cursor2 is \(self.cursor)")
        return self.screen
    }
    
    override func isLockedWidget() -> Bool {
        // the widget is locked when manuevering inside, which means it gets most events and not the screen
        return self.locked
    }

    override func onOtherKey(key: Int32) -> BaseScreen {
        if (key == 9) {
            // tab
            self.locked = !(self.locked)
            redraw()
        }
        return self.screen
    }

    override func getCustomDisplayHandler() -> ((Int) -> Void)? {
        return widgetRenderer
    }

    func widgetRenderer(_ screenRow: Int) {
        let start : Int = startMessage()
        let end : Int = endMessage()
        var index : Int = 0
        if (!self.locked) {
            // FIXME: fields should know when they are selected, and they could print slightly better messages
            self.screen.print(row: screenRow + index, col: 10, text: "--- select + tab to operate ----------")
        }
        else {
            self.screen.print(row: screenRow + index, col: 10, text: "--- up/down to scroll, tab to exit ---")

        }
        for message in self.messages[start ... end] {
            self.screen.print(row: screenRow + 1 + index, col: 10, text: String(repeating: " ", count: 60))
            self.screen.print(row: screenRow + 1 + index, col: 10, text: "| \(message)")
            index = index + 1
        }
        
    }
    
    func startMessage() -> Int {
        let result : Int = self.cursor
        if (self.cursor < 0) {
            return 0
        }
        return result
    }
    
    func endMessage() -> Int {
        let result = self.cursor + self.messagesPerScreen
        if (result > self.messages.count - 1) {
            return self.messages.count - 1
        } else {
            return result
        }
    }
    
    @inline(__always) func redraw() -> Void {
        // just re-render the widget
        widgetRenderer(self.screenRow)
    }
    
}
