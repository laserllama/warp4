//
//  Paginator.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/12/21.
//

import Foundation


class Paginator<ValueType : Browseable> : BaseField {
    
    var collection: [ValueType]
    var pageSize : Int
    var pagesRequired : Int
    var page: Int
    var pages : [Page<ValueType>] = []
    var editFunc : (ValueType) -> BaseScreen
    
    init(screen: BaseScreen, collection: [ValueType ], pageSize: Int, editFunc: @escaping (ValueType) -> BaseScreen) {
        self.collection = collection
        self.pageSize = pageSize
        self.pagesRequired = collection.count / pageSize
        self.page = 0
        self.editFunc = editFunc
        super.init(screen: screen)
        reloadPages()
    }
    
    func reloadPages() {
        var offset : Int = self.page * self.pageSize
        for page in self.pages {
            page.setObject(index: offset)
            offset = offset + 1
        }
    }
    
    override func onLeft() -> BaseScreen {
        if (self.page > 0) {
            self.page -= 1
            reloadPages()
            self.screen.invalidate()
        }
        return self.screen
    }
    
    override func onRight() -> BaseScreen {
        if (self.page < self.pagesRequired) {
            self.page += 1
            reloadPages()
            self.screen.invalidate()
        }
        return self.screen
    }
    
    override func getDisplayString() -> String {
        return "Page : < \(self.page+1) / \(self.pagesRequired + 1) >"
    }
    
    func insertFields(results: inout [BaseField]) {
    
        var short : Bool = false
        
        if (self.collection.count > self.pageSize) {
            // no need to include the actual page control and blank space when the number of objects is less than the page size
            results.append(self)
            results.append(BlankSpace(screen: self.screen))
        } else {
            short = true
        }
    
        for i in 0 ... pageSize {
            if ((!short) || (i < self.collection.count)) {
                let page = Page<ValueType>(screen: self.screen, paginator: self, index: i)
                page.setObject(index: i)
                pages.append(page)
                results.append(page)
            }
        }
        
    }
    
    
}
