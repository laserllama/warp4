//
//  NavField.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/4/21.
//

import Foundation

//
//  MenuChoice.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/4/21.
//

import Foundation

class NavField : BaseField {
    
    var label: String
    var navigateTo : BaseScreen
    
    init(screen: BaseScreen, label: String, navigateTo: BaseScreen) {
        self.label = label
        self.navigateTo = navigateTo
        super.init(screen: screen)
    }
        
    override func onEnter() -> BaseScreen {
        return self.navigateTo
    }
    
    override func getDisplayString() -> String {
        return self.label
    }
    
}
