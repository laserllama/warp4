//
//  TextField.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/6/21.
//

import Foundation

class TextField : BaseField {
    
    var label: String
    var value: String
    var initialValue: String
    var cursorPosition: Int
    var maxLength: Int
    var enterHandler: ((_ value: String) -> BaseScreen)?
        
    var action: (_ value: String) -> Bool
        
    init(screen: BaseScreen, label: String, value: String, maxLength: Int, enterHandler: ((_ value: String) -> BaseScreen)? = nil, action: @escaping (_ value: String) -> Bool) {
        self.label = label
        self.action = action
        self.value = value
        self.initialValue = value
        self.cursorPosition = self.value.count + 1
        self.maxLength = maxLength
        self.enterHandler = enterHandler
        
        super.init(screen: screen)
        
        self.hasValue = true
        self.valueLeftBracket = "["
        self.valueRightBracket = "]"
        self.valueDisplayWidth = maxLength + 5
        
    }
    
    private func doApply() {
        if (!self.action(self.value)) {
            // show we have an error
            // FIXME: later we may have some flash() capability.
            self.valueLeftBracket = "!"
            self.valueRightBracket = "!"
        } else {
            self.valueLeftBracket = "["
            self.valueRightBracket = "]"
        }
    }
    
    override func onEnter() -> BaseScreen {
        if (self.enterHandler != nil) {
            return self.enterHandler!(self.value)
        }
        return self.screen
    }
     
    override func onBackspace() -> BaseScreen {
        if (self.value.count > 0) {
            self.value.removeLast()
            self.doApply()
            self.screen.drawFieldText(field: self)
        }
        return self.screen
    }
    
    override func onRegularCharacter(key: Int32) -> BaseScreen {
        if (self.value.count >= maxLength) {
            return self.screen
        }
        let letter = String(UnicodeScalar(UInt8(key)))
        self.value.append(letter)
        self.doApply()
        self.screen.drawFieldText(field: self)
        return self.screen
    }
    
    override func getDisplayString() -> String {
        return self.label
    }
    
    override func getDisplayValue() -> String {
        return self.value
    }
    
}

