//
//  MenuHeader.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/4/21.
//

import Foundation

class ScreenLabel {
 
    var text: String
    var screenRow: Int = 0
    
    init(text: String) {
        self.text = text
    }
    
    func setLocation(screenRow: Int) {
        self.screenRow = screenRow
    }
    
    func getDisplayString() -> String {
        return self.text
    }
    
}
