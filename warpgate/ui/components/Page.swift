//
//  Page.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/12/21.
//

import Foundation

class Page<ValueType : Browseable> : BaseField{
    
    var paginator: Paginator<ValueType>
    var index: Int
    var displayValue: String = ""
    var selectable : Bool = true
    var locked : Bool = false
    var object : ValueType? = nil
    
    init(screen: BaseScreen, paginator: Paginator<ValueType>, index: Int) {
        self.paginator = paginator
        self.index = index
        super.init(screen: screen)
    }
    
    func setObject(index: Int) {
        self.index = index
        if (index < self.paginator.collection.count) {
            self.object = self.paginator.collection[index]
            self.displayValue = object!.getInterfaceDisplayString()
            self.selectable = true
        } else {
            self.object = nil
            self.displayValue = ""
            self.selectable = false
        }
    }
    
    override func isSelectable() -> Bool {
        return self.selectable
    }
    
    override func getDisplayString() -> String {
        return self.displayValue
    }
    
    override func onEnter() -> BaseScreen {
        if (self.object != nil) {
            WarpLogger.log(message: "onEnter at \(index) is not nil")
            return paginator.editFunc(self.object!)
        }
        else {
            WarpLogger.log(message: "onEnter at \(index) is nil")
        }
        return self.screen
    }
    
    override func isLockedWidget() -> Bool {
        return self.locked
    }
    
    
    override func onKeyDownWhenLocked() -> BaseScreen {
        // move it down in the collection
        return self.screen
    }
    
    override func onKeyUpWhenLocked() -> BaseScreen {
        // move it up in the collection
        return self.screen
    }
    
    override func onOtherKey(key: Int32) -> BaseScreen {
        if (key == 46) { // .
            self.locked = !self.locked
        }
        return self.screen
    }
}
