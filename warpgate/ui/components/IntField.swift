
//
//  TextField.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/6/21.
//

import Foundation

class IntField : BaseField {
    
    var label: String
    var value: Int
    var min: Int
    var max: Int
        
    var action: (_ value: Int) -> Bool
        
    init(screen: BaseScreen, label: String, value: Int, min: Int, max: Int, action: @escaping (_ value: Int) -> Bool) {
        self.label = label
        self.action = action
        self.value = value
        self.min = min
        self.max = max
            
        super.init(screen: screen)
        
        self.hasValue = true
        self.valueLeftBracket = "<"
        self.valueRightBracket = ">"
        self.valueDisplayWidth = 35
        
    }
    
    private func doApply() {
        let _ = self.action(self.value)
        self.screen.drawFieldText(field: self)
    }
     
    override func onLeft() -> BaseScreen {
        if (self.value > self.min) {
            self.value = self.value - 1
        }
        self.doApply()
        return self.screen
    }
    
    override func onRight() -> BaseScreen {
        if (self.value < self.max) {
            self.value = self.value + 1
        }
        self.doApply()
        return self.screen
    }
    
    override func getDisplayString() -> String {
        return self.label
    }
    
    override func getDisplayValue() -> String {
        return "\(self.value)"
    }
    
}

