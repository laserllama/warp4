//
//  InstrumentScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//

import Foundation

class InstrumentScreen : BaseScreen {
    
    var set: WarpSet
    var instrument: WarpInstrument
    
    init(set: WarpSet, instrument: WarpInstrument, back: BaseScreen) {
        self.set = set
        self.instrument = instrument
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Instrument: \(instrument.name)")
        ]
        
    }
    
    override func getFields() -> [BaseField] {
        
        //var deviceChoices: [String:String] = [:]
        //for device in self.set.devices {
        //    deviceChoices[device.name] = device.name
        //}
        
        return [
            
            TextField(screen: self, label: "Name", value: self.instrument.name, maxLength: 30) {
                if ($0 == self.instrument.name) { return true }
                if (WarpInstrument.nameExists(set: self.set, newName: $0)) { return false; }
                self.instrument.rename(set: self.set, newName: $0);
                return true
            },
                        
            IntField(screen: self, label: "Channel", value: self.instrument.channel, min: 1, max: 16) {
                self.instrument.channel = $0
                return true
            },
            
            SelectField<WarpDevice>(screen: self, label: "Device", value: self.set.getDevice(name: self.instrument.deviceName)!, choices: self.set.devices, toString: { $0.name }) {
                self.instrument.deviceName = $0.name
            },
            
            BlankSpace(screen: self),
            
            IntField(screen: self, label: "Base Octave", value: self.instrument.baseOctave, min: -2, max: 7) {
                self.instrument.baseOctave = $0
                return true
            },
            
            BooleanField(screen: self, label: "Muted", value: self.instrument.muted) {
                self.instrument.muted = $0
            },
            
            BooleanField(screen: self, label: "Percussion", value: self.instrument.percussion) {
                self.instrument.percussion = $0
            },
            
            NavField(screen: self, label: "Percussion Map...", navigateTo: PercussionMapScreen(set: self.set, instrument: self.instrument, back: self)),
            
            BlankSpace(screen: self),
            
            NavField(screen: self, label: "(Back)", navigateTo: self.back!),
        ]
    }
    
}
