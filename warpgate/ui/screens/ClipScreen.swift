
import Foundation

class ClipScreen : BaseScreen {
    
    var set: WarpSet
    var song: WarpSong
    var scene: WarpScene
    var clip: WarpClip
    
    init(set: WarpSet, song: WarpSong, scene: WarpScene, clip: WarpClip, back: BaseScreen) {
        self.set = set
        self.song = song
        self.scene = scene
        self.clip = clip
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Song: \(song.name) / Scene: \(scene.name) / Clip: \(clip.trackName)")
        ]
        
    }
    
    override func getFields() -> [BaseField] {
        
        //var deviceChoices: [String:String] = [:]
        //for device in self.set.devices {
        //    deviceChoices[device.name] = device.name
        //}
        
        var patternChoices : [String ] = self.song.patterns.map { $0.name }
        patternChoices.insert("-", at: 0)
        
        var arpChoices : [String ] = self.song.arps.map { $0.name }
        arpChoices.insert("-", at: 0)
        
        return [
            

            StringField(screen:self, label: "Patterns (Sequential):"),
            SelectField<String>(screen: self, label: "1: ", value: self.clip.pattern1Name, choices: patternChoices, toString: { $0 }) {
                self.clip.pattern1Name = $0
            },
            SelectField<String>(screen: self, label: "2: ", value: self.clip.pattern2Name, choices: patternChoices, toString: { $0 }) {
                self.clip.pattern2Name = $0
            },
            SelectField<String>(screen: self, label: "3: ", value: self.clip.pattern3Name, choices: patternChoices, toString: { $0 }) {
                self.clip.pattern3Name = $0
            },
            SelectField<String>(screen: self, label: "4: ", value: self.clip.pattern4Name, choices: patternChoices, toString: { $0 }) {
                self.clip.pattern4Name = $0
            },
            BlankSpace(screen: self),
            
            StringField(screen:self, label: "Arps (Stacked):"),
            SelectField<String>(screen: self, label: "1: ", value: self.clip.arp1Name, choices: arpChoices, toString: { $0 }) {
                self.clip.arp1Name = $0
            },
            SelectField<String>(screen: self, label: "2: ", value: self.clip.arp2Name, choices: arpChoices, toString: { $0 }) {
                self.clip.arp2Name = $0
            },
            SelectField<String>(screen: self, label: "3: ", value: self.clip.arp3Name, choices: arpChoices, toString: { $0 }) {
                self.clip.arp3Name = $0
            },
            SelectField<String>(screen: self, label: "4: ", value: self.clip.arp4Name, choices: arpChoices, toString: { $0 }) {
                self.clip.arp4Name = $0
            },
            BlankSpace(screen: self),
            
            
            SelectField<Double>(screen: self, label: "Tempo", value: self.scene.tempoMultiplier, choices: TEMPO_MULTIPLIER_CHOICES, toString: { String($0) }, rotate: false) {
                self.scene.tempoMultiplier = $0
            },
            BlankSpace(screen: self),
             
             
            NavField(screen: self, label: "(Back)", navigateTo: self.back!),
            
        ]
    }
    
}
