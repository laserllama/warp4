//
//  ScenesScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//


import Foundation

class ScenesScreen : BaseScreen {
    
    var set: WarpSet
    var song: WarpSong
    
    init(set: WarpSet, song: WarpSong, back: BaseScreen) {
        self.set = set
        self.song = song
        super.init(back: back)
    }
        
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Song \(song.name) / Scenes")
        ]
    }
    
    func editScene(scene: WarpScene) -> BaseScreen {
        return SceneScreen(set: self.set, song: self.song, scene: scene, back: self)
    }
    
    override func getFields() -> [BaseField] {
        
        var results : [BaseField] = []

        let paginator = Paginator<WarpScene>(screen: self, collection: self.song.scenes, pageSize: 10, editFunc: editScene)
        paginator.insertFields(results: &results)
        results.append(BlankSpace(screen: self))
        
        // TODO: add some indicator of the scene index.
                        
        results.append(
            ActionField(screen: self, label: "[New Scene]", reloadOnEnter: true) {
                let newScene : WarpScene = self.song.addNewScene(set: self.set)
                return SceneScreen(set: self.set, song: self.song, scene: newScene, back: self)
            }
        )
        results.append(BlankSpace(screen: self))

        // TODO: if up/down via "." is not working well, consider adding it here.
        // TODO: add a copyScene button that copies all the clips
        
        results.append(
            NavField(screen: self, label: "(Back)", navigateTo: self.back!)
        )
        
        return results
        
    }
    
}

