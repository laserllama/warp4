//
//  PatternScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//

import Foundation


class PatternScreen : BaseScreen {
    
    var set: WarpSet
    var song: WarpSong
    var pattern: WarpPattern
    var slotProperty: String = "note"
    
    init(set: WarpSet, song: WarpSong, pattern: WarpPattern, back: BaseScreen) {
        self.set = set
        self.song = song
        self.pattern = pattern
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Song: \(song.name) / Pattern: \(pattern.name)")
        ]
        
    }
    
    func copyFieldEnterHandler(newName: String) -> BaseScreen {
        
        if (newName == "") {
            return self
        }
        
        let newPattern : WarpPattern = self.pattern.copy(newName: newName)
        self.song.patterns.append(newPattern)
        return PatternScreen(set: self.set, song: self.song, pattern: newPattern, back: self.back!)
    }
    
    override func getFields() -> [BaseField] {
        
        let widget = SlotEditorWidget<WarpPattern>(screen: self, song: self.song, container: self.pattern, property: "note", forPattern: true)
        
        var instrumentChoices : [String] = self.set.instruments.map { $0.name }
        instrumentChoices.insert("-", at: 0)

        var results : [BaseField] = [
            
            TextField(screen: self, label: "Name", value: self.pattern.name, maxLength: 30) {
                if ($0 == self.pattern.name) { return true }
                if (WarpPattern.nameExists(song: self.song, newName: $0)) { return false; }
                self.pattern.name = $0
                return true
            },
            
            SelectField<WarpScale>(screen: self, label: "Scale", value: self.song.getScale(name: self.pattern.scaleName)!, choices: self.song.scales, toString: { $0.name }) {
                self.pattern.scaleName = $0.name
            },
            
            BooleanField(screen: self, label: "Percussion", value: self.pattern.percussion) {
                self.pattern.percussion = $0
            },
            
            IntField(screen: self, label: "Length", value: self.pattern.length, min: 1, max: 64) {
                self.pattern.length = $0
                return true
            },
            
            SelectField<Double>(screen: self, label: "Tempo", value: self.pattern.tempoMultiplier, choices: TEMPO_MULTIPLIER_CHOICES, toString: { String($0) }, rotate: false) {
                self.pattern.tempoMultiplier = $0
            },
            
            IntField(screen: self, label: "Octave Shift", value: self.pattern.octaveShift, min: -5, max: 5) {
                self.pattern.octaveShift = $0
                return true
            },
            
            BooleanField(screen: self, label: "One Shot", value: self.pattern.oneShot) {
                self.pattern.oneShot = $0
            },
            
            SelectField<String>(screen: self, label: "Audition Instrument", value: self.pattern.auditionInstrumentName, choices: instrumentChoices, toString: { $0 }) {
                self.pattern.auditionInstrumentName = $0
            },
            
                    
            widget,
            
            // ButtonBar
            StringField(screen: self, label: "[Audition](FIXME)"),
                        
            // FIXME: add a "Copy" name here, which can be a TextField with a special onEnter handler
            BlankSpace(screen: self),
            
            TextField(screen: self, label: "Copy To", value: self.pattern.name, maxLength: 30, enterHandler: self.copyFieldEnterHandler) {
                if ($0 == self.pattern.name) { return false; }
                if (WarpPattern.nameExists(song: self.song, newName: $0)) { return false; }
                return true
            },
            
        ]
        
        if WarpPattern.canDelete(song: self.song, pattern: self.pattern) {
            results.append(ActionField(screen: self, label: "Delete", reloadOnEnter: false) {
                WarpPattern.delete(song: self.song, pattern: self.pattern)
                return self.back!
            })
        }
            
        results.append(BlankSpace(screen: self))
        results.append(NavField(screen: self, label: "(Back)", navigateTo: self.back!))
        
        return results
    }
    
}

