//
//  QuitScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/4/21.
//

import Foundation

class QuitScreen : BaseScreen {
    
    var set: WarpSet
    
    init(set: WarpSet, back: BaseScreen) {
        self.set = set
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set \(set.name) / Do You Want To Quit?")
        ]
        
    }
    
    override func getFields() -> [BaseField] {
        return [
            // TODO: add a ActionField here, that calls clear and exit.
            NavField(screen: self, label: "No", navigateTo: self.back!),

            ActionField(screen: self, label: "Quit Without Saving", reloadOnEnter: true) {
                WarpRuntimeContext.quit()
                return self // will not hit
            },
            ActionField(screen: self, label: "Save & Quit", reloadOnEnter: true) {
                self.set.save()
                WarpRuntimeContext.quit()
                return self // will not hit
            }

        ]
    }
    
}
