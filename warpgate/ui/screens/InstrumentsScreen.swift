//
//  InstrumentsScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/4/21.
//

import Foundation

class InstrumentsScreen : BaseScreen {
    
    var set: WarpSet
    
    init(set: WarpSet, back: BaseScreen) {
        self.set = set
        super.init(back: back)
    }
        
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Instruments")
        ]
        
    }
    
    func editInstrument(instrument: WarpInstrument) -> BaseScreen {
        return InstrumentScreen(set: self.set, instrument: instrument, back: self)
    }
    
    override func getFields() -> [BaseField] {
        
            
        var results : [BaseField] = []
            
        


        if set.devices.count > 0 {
            
            let paginator = Paginator<WarpInstrument>(screen: self, collection: self.set.instruments, pageSize: 10, editFunc: editInstrument)
            paginator.insertFields(results: &results)
            results.append(BlankSpace(screen: self))
            
            results.append(
                ActionField(screen: self, label: "[New Instrument]", reloadOnEnter: true) {
                    let newInstrument = self.set.addNewInstrument()
                    return InstrumentScreen(set: self.set, instrument: newInstrument!, back: self)
                }
            )
            
        } else {
            results.append(
                StringField(screen: self, label: "WARNING: No MIDI Devices Found. Connect MIDI Devices and Restart.")
            )
        }
        
        results.append(BlankSpace(screen: self))

            
        results.append(
            NavField(screen: self, label: "(Back)", navigateTo: self.back!)
        )
        
        return results
        
    }
    
}
