//  SetScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/4/21.
//

import Foundation

class BaseScreen {
    
    var back : BaseScreen?
    var cursorPosition: Int = 0
    var headerDisplayOffset : Int = 0
    var fieldDisplayOffset : Int = 0
    var selectedField : Int32 = 0
    var invalid : Bool = false
    
    init(back: BaseScreen?) {
        self.back = back
    }
         
    func onRegularCharacter(key: Int32) -> BaseScreen {
        if key == Int32(UnicodeScalar("l").value) {
            return LogScreen(back: self)
        }
        
        return self
    }
    
    func invalidate() {
        self.invalid = true
    }
    
    func getPreviousField(fields: [BaseField], currentField: BaseField) -> BaseField? {
        var looped : Bool = false
        var result : BaseField = currentField
        if (fields.count == 0) {
            return nil
        }
        while (true) {
            if (result === fields.first!) {
                if (looped) { return nil }
                result = fields.last!
                looped = true
            } else {
                let index : Int = fields.firstIndex(where: { $0 === result })!
                result = fields[index-1]
            }
            if (result.isSelectable()) {
                return result
            }
        }
    }
    
    func getNextField(fields: [BaseField], currentField: BaseField) -> BaseField? {
        var looped : Bool = false
        var result : BaseField = currentField
        if (fields.count == 0) {
            return nil
        }
        while (true) {
            if (result === fields.last!) {
                if (looped) { return nil }
                result = fields.first!
                looped = true
            } else {
                let index : Int = fields.firstIndex(where: { $0 === result })!
                result = fields[index+1]
            }
            if (result.isSelectable()) {
                return result
            }
        }
    }
    
    func getHeaders() -> [ScreenLabel] {
        return []
    }
    
    func getFields() -> [BaseField] {
        return []
    }
    
    func setup() {
        initscr()                   // Init window. Must be first
        cbreak()
        noecho()                    // Don't echo user input
        nonl()                      // Disable newline mode
        intrflush(stdscr, true)     // Prevent flush
        keypad(stdscr, true)        // Enable function and arrow keys
        curs_set(0)                 // Set cursor to invisible
    }
    
    func drawTitle() -> Void {
        move(0,0)
        addstr("Warp Sequencer")
        move(1,0)
        addstr("--------------")
    }
        
    func drawScreen(headers: [ScreenLabel], fields: [BaseField]) {
        
        self.setup()
        clear()
        
        var headerIndex : Int = 0
        var fieldIndex : Int = 0
        
        drawTitle();
    
        self.headerDisplayOffset = 3
        self.fieldDisplayOffset = headers.count + self.headerDisplayOffset + 1

        for (headerItem) in headers {
            headerItem.setLocation(screenRow: headerIndex + self.headerDisplayOffset)
            let displayValue = headerItem.getDisplayString()
            self.drawHeaderText(displayValue: displayValue, screenRow: headerIndex)
            headerIndex += 1
        }
        
        // FIXME: we need to support scrolling through fields if there is more than one.
                
        for (fieldItem) in fields {
            fieldItem.setLocation(screenRow: fieldIndex + self.fieldDisplayOffset)
            self.drawFieldText(field: fieldItem)
            fieldIndex += fieldItem.getDisplayHeight()
        }
        
        refresh()
        
    }
    
    func drawMovedCursor(fromField: BaseField?, toField: BaseField?) {
        
        if ((fromField == nil) || (toField == nil)) {
            return
        }
                
        let fromRow = fromField!.screenRow
        let toRow = toField!.screenRow
        
        if (fromRow != toRow) {
            move(Int32(fromRow), 0)
            addstr("-")
        }
        move(Int32(toRow), 0)
        addstr(">")
        refresh()
    }
    
    func drawCursor(field: BaseField?) {
        self.drawMovedCursor(fromField: field, toField: field)
    }
    
    func drawHeaderText(displayValue: String, screenRow: Int) {
        move(Int32(screenRow + self.headerDisplayOffset), 0)
        addstr(displayValue)
    }
       
    func drawFieldText(field: BaseField) {
        
        let selectable = field.isSelectable()
        let customDisplayHandler : ((_ screenRow: Int) -> Void)? = field.getCustomDisplayHandler()
        
        move(Int32(field.screenRow), 0)
        
        if (selectable) {
            addstr("-")
        }
        else {
            addstr(" ")
        }
        
        if (customDisplayHandler == nil) {
            move(Int32(field.screenRow), 3)
            addstr(field.getDisplayString())
        
            if field.hasValue {
                move(Int32(field.screenRow), Int32(field.valueColumn))
                addstr(String(repeating: " ", count: field.valueDisplayWidth + 4))
                move(Int32(field.screenRow), Int32(field.valueColumn))
                let valueStr = field.getDisplayValue()
            
                addstr("\(field.valueLeftBracket) \(valueStr)")
                move(Int32(field.screenRow), Int32(field.valueColumn + field.valueDisplayWidth))
                addstr(field.valueRightBracket)
            }
        }
        
        else {
            customDisplayHandler!(field.screenRow)
        }
    }
    
    func print(row: Int, col: Int, text: String) {
        move(Int32(row), Int32(col))
        addstr(text)
    }
       
}


