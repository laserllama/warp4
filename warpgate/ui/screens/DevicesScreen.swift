//
//  DevicesScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/4/21.
//

import Foundation

class DevicesScreen : BaseScreen {
    
    var set: WarpSet
    
    init(set: WarpSet, back: BaseScreen) {
        self.set = set
        super.init(back: back)
    }
        
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Devices")
        ]
        
    }
    
    func editDevice(device: WarpDevice) -> BaseScreen {
        // nothing to edit
        return self
    }
       
    override func getFields() -> [BaseField] {
        var results : [BaseField] = []

        if set.devices.count == 0 {
            results.append(StringField(screen: self, label: "WARNING: No MIDI Devices Found. Connect MIDI Devices and Restart."))
        }
        if set.devices.count > 0 {
            let paginator = Paginator<WarpDevice>(screen: self, collection: self.set.devices, pageSize: 10, editFunc: editDevice)
            paginator.insertFields(results: &results)
            
            results.append(BlankSpace(screen: self))
        }
        results.append(
            NavField(screen: self, label: "(Back)", navigateTo: self.back!)
        )
        return results
    }
    
}
