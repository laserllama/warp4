//
//  ScaleScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//

import Foundation


class ScaleScreen : BaseScreen {
    
    var set: WarpSet
    var song: WarpSong
    var scale: WarpScale
    
    init(set: WarpSet, song: WarpSong, scale: WarpScale, back: BaseScreen) {
        self.set = set
        self.song = song
        self.scale = scale
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Song: \(song.name) / Scale: \(scale.name)")
        ]
        
    }
    
    override func getFields() -> [BaseField] {
        
        var results : [BaseField] = [
            
            TextField(screen: self, label: "Name", value: self.scale.name, maxLength: 30) {
                if ($0 == self.scale.name) { return true }
                if (WarpScale.nameExists(song: self.song, newName: $0)) { return false; }
                self.scale.name = $0
                return true
            },

            SelectField<ScaleType>(screen: self, label: "Scale Type", value: self.scale.scaleType, choices: SCALE_TYPES, toString: { $0.rawValue }) {
                self.scale.scaleType = $0
            },
            
            
            IntField(screen: self, label: "Octave", value: self.scale.root.octave, min: -2, max: 8) {
                self.scale.root.octave = $0
                return true
            },
        ]
        
        if WarpScale.canDelete(song: self.song, scale: self.scale) {
            results.append(ActionField(screen: self, label: "Delete", reloadOnEnter: false) {
                WarpScale.delete(song: self.song, scale: self.scale)
                return self.back!
            })
        }
            
        results.append(BlankSpace(screen: self))
        results.append(NavField(screen: self, label: "(Back)", navigateTo: self.back!))
        
        return results
    }
    
}
