//
//  ArpsScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//

import Foundation

class ArpsScreen : BaseScreen {
    
    var set: WarpSet
    var song: WarpSong
    
    init(set: WarpSet, song: WarpSong, back: BaseScreen) {
        self.set = set
        self.song = song
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Song: \(song.name) / Arps")
        ]
        
    }
    
    func editArp(arp: WarpArp) -> BaseScreen {
        return ArpScreen(set: self.set, song: self.song, arp: arp, back: self)
    }
    
    override func getFields() -> [BaseField] {
        
        var results : [BaseField] = []
        
        // FIXME: implement Comparable so we can use .sorted() here
        
        let paginator = Paginator<WarpArp>(screen: self, collection: self.song.arps, pageSize: 10, editFunc: editArp)
        paginator.insertFields(results: &results)
        
        results.append(BlankSpace(screen: self))
        
        results.append(
            ActionField(screen: self, label: "[New Arp]", reloadOnEnter: true) {
                let newArp = self.song.addNewArp()
                return ArpScreen(set: self.set, song: self.song, arp: newArp, back: self)
            }
        )
        results.append(BlankSpace(screen: self))
        results.append(
            NavField(screen: self, label: "(Back)", navigateTo: self.back!)
        )
        return results
    }
    
}



