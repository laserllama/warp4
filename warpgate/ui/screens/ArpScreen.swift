//
//  ArpScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//

import Foundation


class ArpScreen : BaseScreen {
    
    var set: WarpSet
    var song: WarpSong
    var arp: WarpArp
    var slotProperty: String = "note"
    
    init(set: WarpSet, song: WarpSong, arp: WarpArp, back: BaseScreen) {
        self.set = set
        self.song = song
        self.arp = arp
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Song: \(song.name) / Arp: \(arp.name)")
        ]
        
    }
    
    func copyFieldEnterHandler(newName: String) -> BaseScreen {
        
        if (newName == "") {
            return self
        }
        
        let newArp : WarpArp = self.arp.copy(newName: newName)
        self.song.arps.append(newArp)
        return ArpScreen(set: self.set, song: self.song, arp: newArp, back: self.back!)
    }
    
    override func getFields() -> [BaseField] {
        
        let widget = SlotEditorWidget<WarpArp>(screen: self, song: self.song, container: self.arp, property: "note", forPattern: false)
        
        // FIXME: add alphabetical sorting (on all SelectFields, not just here)
        var instrumentChoices : [String] = set.instruments.map { $0.name }
        var patternChoices : [String] = song.patterns.map { $0.name }
        patternChoices.insert("-", at: 0)
        instrumentChoices.insert("-", at: 0)
        
        
        var results : [BaseField] = [
            
            TextField(screen: self, label: "Name", value: self.arp.name, maxLength: 30) {
                if ($0 == self.arp.name) { return true }
                if (WarpArp.nameExists(song: self.song, newName: $0)) { return false; }
                self.arp.name = $0
                return true
            },
            
            IntField(screen: self, label: "Length", value: self.arp.length, min: 1, max: 64) {
                self.arp.length = $0
                return true
            },
            
            SelectField<Double>(screen: self, label: "Speed", value: self.arp.speed, choices: ARP_SPEED_CHOICES, toString: { String($0) }, rotate: false) {
                self.arp.speed = $0
            },
            
            IntField(screen: self, label: "Octave Shift", value: self.arp.octaveShift, min: -5, max: 5) {
                self.arp.octaveShift = $0
                return true
            },
            
            SelectField<String>(screen: self, label: "Audition Instrument", value: self.arp.auditionInstrumentName, choices: instrumentChoices, toString: { $0 }) {
                self.arp.auditionInstrumentName = $0
            },
            
            SelectField<String>(screen: self, label: "Audition Pattern", value: self.arp.auditionPatternName, choices: patternChoices, toString: { $0 }) {
                self.arp.auditionPatternName = $0
            },
                    
            widget,
                                    
            // FIXME: add a "Copy" name here, which can be a TextField with a special onEnter handler
            BlankSpace(screen: self),
            
            TextField(screen: self, label: "Copy To", value: self.arp.name, maxLength: 30, enterHandler: self.copyFieldEnterHandler) {
                if ($0 == self.arp.name) { return false; }
                if (WarpArp.nameExists(song: self.song, newName: $0)) { return false; }
                return true
            },
            
        ]
        
        if WarpArp.canDelete(song: self.song, arp: self.arp) {
            results.append(ActionField(screen: self, label: "Delete", reloadOnEnter: false) {
                WarpArp.delete(song: self.song, arp: self.arp)
                return self.back!
            })
        }
            
        results.append(BlankSpace(screen: self))
        results.append(NavField(screen: self, label: "(Back)", navigateTo: self.back!))
        
        return results
    }
    
}


