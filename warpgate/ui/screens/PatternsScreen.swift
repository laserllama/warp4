//
//  ScalesScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//

import Foundation

class PatternsScreen : BaseScreen {
    
    var set: WarpSet
    var song: WarpSong
    
    init(set: WarpSet, song: WarpSong, back: BaseScreen) {
        self.set = set
        self.song = song
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Song: \(song.name) / Patterns")
        ]
        
    }
    
    func editPattern(pattern: WarpPattern) -> BaseScreen {
        return PatternScreen(set: self.set, song: self.song, pattern: pattern, back: self)
    }
    
    override func getFields() -> [BaseField] {
        
        var results : [BaseField] = []
        
        // FIXME: implement Comparable so we can use .sorted() here
        
        let paginator = Paginator<WarpPattern>(screen: self, collection: self.song.patterns, pageSize: 10, editFunc: editPattern)
        paginator.insertFields(results: &results)
        
        results.append(BlankSpace(screen: self))
        
        results.append(
            ActionField(screen: self, label: "[New Pattern]", reloadOnEnter: true) {
                let newPattern = self.song.addNewPattern()
                return PatternScreen(set: self.set, song: self.song, pattern: newPattern, back: self)
            }
        )
        results.append(BlankSpace(screen: self))
        results.append(
            NavField(screen: self, label: "(Back)", navigateTo: self.back!)
        )
        return results
    }
    
}


