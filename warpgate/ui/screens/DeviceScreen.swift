//
//  DeviceScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//

import Foundation

class DeviceScreen : BaseScreen {
    
    var set: WarpSet
    var device: WarpDevice
    
    init(set: WarpSet, device: WarpDevice, back: BaseScreen) {
        self.set = set
        self.device = device
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Device: \(device.name)")
        ]
        
    }
    
    override func getFields() -> [BaseField] {
        return [
            BooleanField(screen: self, label: "Muted", value: self.device.muted) {
                self.device.muted = $0
            },
            BlankSpace(screen: self),
            NavField(screen: self, label: "(Back)", navigateTo: self.back!),
        ]
    }
    
}

