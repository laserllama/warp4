//
//  SongScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//

import Foundation

class SongScreen : BaseScreen {
    
    var set: WarpSet
    var song: WarpSong
    
    init(set: WarpSet, song: WarpSong, back: BaseScreen) {
        self.set = set
        self.song = song
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Song: \(song.name)")
        ]
        
    }
    
    override func getFields() -> [BaseField] {
        
        // only the song object cannot have an inherited scale
        let scaleChoices = self.song.scales.filter({ $0.scaleType != ScaleType.inherit })
        
        return [
            TextField(screen: self, label: "Name", value: self.song.name, maxLength: 30) {
                if ($0 == self.song.name) { return true }
                if (WarpSong.nameExists(set: self.set, newName: $0)) { return false; }
                self.song.name = $0
                return true
            },
            
            SelectField<WarpScale>(screen: self, label: "Scale", value: self.song.getScale(name: self.song.scaleName)!, choices: scaleChoices, toString: { $0.name }) {
                self.song.scaleName = $0.name
            },
 
            IntField(screen: self, label: "Tempo", value: self.song.tempo, min: 10, max: 400) {
                self.song.tempo = $0
                return true
            },
            BooleanField(screen: self, label: "Auto Advance Scene", value: self.song.autoAdvanceScenes) {
                self.song.autoAdvanceScenes = $0
            },
            
            BlankSpace(screen: self),

            NavField(screen: self, label: "Scales (\(self.song.scales.count - 1))", navigateTo: ScalesScreen(set: self.set, song: self.song, back: self)),
            NavField(screen: self, label: "Patterns (\(self.song.patterns.count))", navigateTo: PatternsScreen(set: self.set, song: self.song, back: self)),
            NavField(screen: self, label: "Arps (\(self.song.arps.count))", navigateTo: ArpsScreen(set: self.set, song: self.song, back: self)),
            BlankSpace(screen: self),
            NavField(screen: self, label: "Scenes (\(self.song.scenes.count))", navigateTo: ScenesScreen(set: self.set, song: self.song, back: self)),
            NavField(screen: self, label: "Tracks (\(self.song.tracks.count))", navigateTo: TracksScreen(set: self.set, song: self.song, back: self)),
            BlankSpace(screen: self),
            NavField(screen: self, label: "(Back)", navigateTo: self.back!),
        ]
    }
    
}
