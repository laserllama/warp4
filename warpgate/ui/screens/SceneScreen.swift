//
//  SceneScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//


import Foundation

class SceneScreen : BaseScreen {
    
    var set: WarpSet
    var song: WarpSong
    var scene: WarpScene
    
    init(set: WarpSet, song: WarpSong, scene: WarpScene, back: BaseScreen) {
        self.set = set
        self.song = song
        self.scene = scene
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Song: \(song.name) / Scene: \(scene.name)")
        ]
        
    }
    
    func editClip(clip: WarpClip) -> BaseScreen {
        return ClipScreen(
            set: self.set, song: self.song, scene: self.scene, clip: clip, back: self
        )
    }
    
    override func getFields() -> [BaseField] {

        let scaleChoices : [WarpScale] = self.song.scales
        
        var results : [BaseField] = [
            
            TextField(screen: self, label: "Name", value: self.scene.name, maxLength: 30) {
                if ($0 == self.scene.name) { return true }
                if (WarpScene.nameExists(song: self.song, newName: $0)) { return false; }
                self.scene.rename(song: self.song, newName: $0);
                return true
            },
            
            SelectField<WarpScale>(screen: self, label: "Scale", value: self.song.getScale(name: self.song.scaleName)!, choices: scaleChoices, toString: { $0.name }) {
                self.scene.scaleName = $0.name
            },
            
            SelectField<Double>(screen: self, label: "Tempo", value: self.scene.tempoMultiplier, choices: TEMPO_MULTIPLIER_CHOICES, toString: { String($0) }, rotate: false) {
                self.scene.tempoMultiplier = $0
            },
            
            IntField(screen: self, label: "Bars", value: self.scene.bars, min: 0, max: 128) {
                self.scene.bars = $0
                return true
            },
            
            BooleanField(screen: self, label: "Enabled", value: self.scene.enabled) {
                self.scene.enabled = $0
            },
 
        ]
        
        
        results.append(BlankSpace(screen: self))
        
        // ensure there is a clip object for every track - and none that shouldn't be there
        // this should already be done when adding/deleting the track already
        
        self.scene.normalizeClips(song: self.song)
        
        var ok : Bool = true
        
        if self.scene.clips.count == 0 {
            results.append(StringField(screen: self, label: "[!] Add some tracks before proceeding."))
            ok = false
        }
        if self.song.patterns.count == 0 {
            results.append(StringField(screen: self, label: "[!] Add some patterns before proceeding."))
            ok = false
        }
        if ok {
            results.append(StringField(screen: self, label: "Clips..."))
            let paginator = Paginator<WarpClip>(screen: self, collection: self.scene.clips, pageSize: 10, editFunc: editClip)
            paginator.insertFields(results: &results)
        }
 
            
      
               
            
        results.append(BlankSpace(screen: self))
            
        results.append(NavField(screen: self, label: "(Back)", navigateTo: self.back!))
        return results
    }
    
}
