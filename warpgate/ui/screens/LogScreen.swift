//
//  LogScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/10/21.
//

import Foundation

class LogScreen : BaseScreen {
        
    init(back: BaseScreen) {
        super.init(back: back)
    }
            
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "*** LOG EVENTS (tab + up/down to scroll) ***")
        ]
    }
    
    override func onRegularCharacter(key: Int32) -> BaseScreen {
        return self
    }

    override func getFields() -> [BaseField] {
        var results : [BaseField] = []
        
        results.append(LogsWidget(screen: self))
        results.append(BlankSpace(screen: self))
        results.append(BlankSpace(screen: self))
        results.append(NavField(screen: self, label: "(Back)", navigateTo: self.back!))
        
        return results
    }
    
}
