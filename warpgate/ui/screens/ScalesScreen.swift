//
//  ScalesScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//

import Foundation

class ScalesScreen : BaseScreen {
    
    var set: WarpSet
    var song: WarpSong
    
    init(set: WarpSet, song: WarpSong, back: BaseScreen) {
        self.set = set
        self.song = song
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Song: \(song.name) / Scales")
        ]
        
    }
    
    func editScale(scale: WarpScale) -> BaseScreen {
        return ScaleScreen(set: self.set, song: self.song, scale: scale, back: self)
    }
    
    override func getFields() -> [BaseField] {
        
        var results : [BaseField] = []
        
        let scaleChoices : [WarpScale] = self.song.scales.filter { $0.scaleType != ScaleType.inherit }

        let paginator = Paginator<WarpScale>(screen: self, collection: scaleChoices, pageSize: 10, editFunc: editScale)
        paginator.insertFields(results: &results)
        results.append(BlankSpace(screen: self))
        
        results.append(
            ActionField(screen: self, label: "[New Scale]", reloadOnEnter: true) {
                let newScale = self.song.addNewScale()
                return ScaleScreen(set: self.set, song: self.song, scale: newScale, back: self)
            }
        )
        results.append(BlankSpace(screen: self))
        results.append(
            NavField(screen: self, label: "(Back)", navigateTo: self.back!)
        )
        return results
    }
    
}

