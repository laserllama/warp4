//
//  SetScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/4/21.
//

import Foundation

class SetScreen : BaseScreen {
    
    var set: WarpSet
    
    init(set: WarpSet) {
        self.set = set
        super.init(back: nil)        
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name)")
        ]
        
    }
    
    override func getFields() -> [BaseField] {
        // FIXME: make hotkeys work (S/D/I/Q) later ...
        return [
            
            TextField(screen: self, label: "Name", value: self.set.name, maxLength: 30) {
                if ($0.count < 1) {
                    return false;
                }
                self.set.name = $0
                return true
            },
            TextField(screen: self, label: "Filename", value: self.set.filename, maxLength: 30) {
                if ($0.count < 1) {
                    return false;
                }
                self.set.filename = $0
                return true
            },
            BlankSpace(screen: self),
            
            NavField(screen: self, label: "Songs (\(self.set.songs.count))", navigateTo: SongsScreen(set: self.set, back: self)),
            
            BlankSpace(screen: self),
            
            NavField(screen: self, label: "Devices (\(self.set.devices.count))", navigateTo: DevicesScreen(set: self.set, back: self)),
            NavField(screen: self, label: "Instruments (\(self.set.instruments.count))", navigateTo: InstrumentsScreen(set: self.set, back: self)),

            BlankSpace(screen: self),
            NavField(screen: self, label: "Open...", navigateTo: OpenScreen(set: self.set, back: self)),
            NavField(screen: self, label: "Save...", navigateTo: SaveScreen(set: self.set, back: self)),
            NavField(screen: self, label: "Quit...", navigateTo: QuitScreen(set: self.set, back: self)),
            BlankSpace(screen: self),

            StringField(screen: self, label: "** press 'h' for help **"),
            BlankSpace(screen: self),
            
            // FIXME: this could go to a FileScreen
            //StringField(screen: self, label: "Open (FIXME)"),
            //StringField(screen: self, label: "Save (FIXME)"),
            //StringField(screen: self, label: "Reset (FIXME)"),
            //BlankSpace(screen: self),
            
            // NavField(screen: self, label: "(Back)", navigateTo: self.back!),

        ]
    }
    
}
