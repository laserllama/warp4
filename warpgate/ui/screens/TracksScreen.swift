//
//  TracksScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//


import Foundation

class TracksScreen : BaseScreen {
    
    var set: WarpSet
    var song: WarpSong
    
    init(set: WarpSet, song: WarpSong, back: BaseScreen) {
        self.set = set
        self.song = song
        super.init(back: back)
    }
        
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Song \(song.name) / Tracks")
        ]
    }
    
    func editTrack(track: WarpTrack) -> BaseScreen {
        return TrackScreen(set: self.set, song: self.song, track: track, back: self)
    }
    
    override func getFields() -> [BaseField] {
        
        var results : [BaseField] = []

        let paginator = Paginator<WarpTrack>(screen: self, collection: self.song.tracks, pageSize: 10, editFunc: editTrack)
        paginator.insertFields(results: &results)
        results.append(BlankSpace(screen: self))
                
        if self.song.tracks.count > 0 {
            results.append(BlankSpace(screen: self))
        }
            
        if set.instruments.count > 0 {
        
            results.append(
                ActionField(screen: self, label: "[New Track]", reloadOnEnter: true) {
                    let newTrack : WarpTrack = self.song.addNewTrack(set: self.set)
                    return TrackScreen(set: self.set, song: self.song, track: newTrack, back: self)
                }
            )
            
        } else {
            results.append(
                StringField(screen: self, label: "TIP: Create some instruments before adding tracks")
            )
        }
        
        results.append(BlankSpace(screen: self))

            
        results.append(
            NavField(screen: self, label: "(Back)", navigateTo: self.back!)
        )
        
        return results
        
    }
    
}
