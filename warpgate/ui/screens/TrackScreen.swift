//
//  TrackScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//


import Foundation

class TrackScreen : BaseScreen {
    
    var set: WarpSet
    var song: WarpSong
    var track: WarpTrack
    
    init(set: WarpSet, song: WarpSong, track: WarpTrack, back: BaseScreen) {
        self.set = set
        self.song = song
        self.track = track
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Song: \(song.name) / Track: \(track.name)")
        ]
        
    }
    
    override func getFields() -> [BaseField] {
        
        //var deviceChoices: [String:String] = [:]
        //for device in self.set.devices {
        //    deviceChoices[device.name] = device.name
        //}
        
        return [
            
            TextField(screen: self, label: "Name", value: self.track.name, maxLength: 30) {
                if ($0 == self.track.name) { return true }
                if (WarpTrack.nameExists(song: self.song, newName: $0)) { return false; }
                self.track.rename(song: self.song, newName: $0);
                return true
            },
                                    
            SelectField<WarpInstrument>(screen: self, label: "Instrument", value: self.set.getInstrument(name: self.track.instrumentName)!, choices: self.set.instruments, toString: { $0.name }) {
                self.track.instrumentName = $0.name
            },
 
            BooleanField(screen: self, label: "Muted", value: self.track.muted) {
                self.track.muted = $0
            },
    
            
            BlankSpace(screen: self),
            
            NavField(screen: self, label: "(Back)", navigateTo: self.back!),
        ]
    }
    
}
