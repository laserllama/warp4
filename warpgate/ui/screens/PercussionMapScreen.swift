//
//  PercussionMapScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/21/21.
//

import Foundation

class PercussionMapScreen : BaseScreen {
    
    var set: WarpSet
    var instrument: WarpInstrument
    
    init(set: WarpSet, instrument: WarpInstrument, back: BaseScreen) {
        self.set = set
        self.instrument = instrument
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Instrument: \(instrument.name) / Percussion Map")
        ]
        
    }
    
    override func getFields() -> [BaseField] {
        
        
        
        // FIXME: this should be a static method in WarpNote ...
        var noteChoices: [WarpNote] = []
        for octave in -2 ... 8 {
            for name in NOTE_NAME_STRINGS {
                noteChoices.append(WarpNote(name: NoteName(rawValue: name)!, octave: octave))
            }
        }

        var results : [BaseField] = []
        
        for i in 0 ... 9 {
            
            results.append(
                SelectField<WarpNote>(screen: self, label: "Note \(i)", value: self.instrument.percussionMap[i], choices: noteChoices, toString: { "\($0.name.rawValue)\($0.octave)" }) {
                    self.instrument.percussionMap[i] = $0
                }
            )
        
        }
        
        results.append(BlankSpace(screen: self))
        results.append(NavField(screen: self, label: "(Back)", navigateTo: self.back!))
        
        return results
    }
    
}
