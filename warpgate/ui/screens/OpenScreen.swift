//
//  OpenScreen.swift
//  warpgate
//
//  Created by Michael DeHaan

import Foundation

class OpenScreen : BaseScreen {
    
    var set: WarpSet
    
    init(set: WarpSet, back: BaseScreen) {
        self.set = set
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "Set \(set.name) / Open File")
        ]
        
    }
    
    func openFile(_ file: WarpFile) -> BaseScreen {
        
        let newSet : WarpSet? = WarpStorageManager.open(filename: file.name)
        if (newSet != nil) {
            return SetScreen(set: WarpRuntimeContext.set)
        } else {
            return self
        }
    }
    
    override func getFields() -> [BaseField] {
        
        var results: [BaseField] = []
        
        let files = WarpStorageManager.getFilesToOpen()
    
        let paginator = Paginator<WarpFile>(screen: self, collection: files, pageSize: 10, editFunc: openFile)
        paginator.insertFields(results: &results)
        results.append(BlankSpace(screen: self))
      
        results.append(NavField(screen: self, label: "(Back)", navigateTo: self.back!))
        
        return results
        
    }
    
}
