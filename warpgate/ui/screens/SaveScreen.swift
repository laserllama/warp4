//
//  SaveScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/14/21.
//

import Foundation

class SaveScreen : BaseScreen {
    
    var set : WarpSet
    
    init(set: WarpSet, back: BaseScreen) {
        self.set = set
        super.init(back: back)
    }
    
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "Warp / Set: \(set.name) / Save File?")
        ]
        
    }
        
    override func getFields() -> [BaseField] {
        
        var results: [BaseField] = []
        
        //let files = WarpStorageManager.getPossibleFiles()
    
        results.append(ActionField(screen: self, label: "Yes", reloadOnEnter: true) {
            self.set.save()
            return self.back!
        })
        
        results.append(NavField(screen: self, label: "No", navigateTo: self.back!))
        
        return results
        
    }
    
}
