//
//  TracksScreen.swift
//  warpgate
//
//  Created by Michael DeHaan on 6/5/21.
//


import Foundation

class SongsScreen : BaseScreen {
    
    var set: WarpSet
    
    init(set: WarpSet, back: BaseScreen) {
        self.set = set
        super.init(back: back)
    }
        
    override func getHeaders() -> [ScreenLabel] {
        return [
            ScreenLabel(text: "@ Set: \(set.name) / Songs")
        ]
    }
    
    func editSong(song: WarpSong) -> BaseScreen {
        return SongScreen(set: self.set, song: song, back: self)
    }
    
    override func getFields() -> [BaseField] {
        
            
        var results : [BaseField] = []
        
        let paginator = Paginator<WarpSong>(screen: self, collection: set.songs, pageSize: 10, editFunc: editSong)
        paginator.insertFields(results: &results)
          
        results.append(BlankSpace(screen: self))

        results.append(
            ActionField(screen: self, label: "[New Song]", reloadOnEnter: true) {
                let newSong : WarpSong = self.set.addNewSong()
                return SongScreen(set: self.set, song: newSong, back: self)
            }
        )
                
        results.append(BlankSpace(screen: self))
            
        results.append(
            NavField(screen: self, label: "(Back)", navigateTo: self.back!)
        )
        
        return results
        
    }
    
}
